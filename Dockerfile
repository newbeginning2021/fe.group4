FROM node:14
WORKDIR /app
COPY package*.json ./
RUN npx browserslist@latest --update-db
RUN npm install --production
COPY . .
CMD ["npm", "start"]