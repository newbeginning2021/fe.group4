import Axios from 'axios'
import { getToken } from '../service/auth'
import baseUrl from '../config/confg';

Axios.defaults.baseURL = 'http://api.ivyschoolmanage.com:8000'
const token = getToken()
if(token) {
    Axios.defaults.headers.common = {'Authorization': `bearer ${token}`}
}

export function reGetToken(){
  const retoken = getToken()
  if(retoken) {
      Axios.defaults.headers.common = {'Authorization': `bearer ${retoken}`}
  }
  
}


export const getUsersLogin = (values) => Axios.post(`${baseUrl}/login`,values)

export const getUsers = (keyword) => Axios.get(`${baseUrl}/user/${keyword}`)

export const postUsers = (accountInfo) => Axios.post(`${baseUrl}/users`,accountInfo)

export const deleteUsers = () => Axios.delete(`${baseUrl}/users/:id`)

export const putUsers = (id, updateInfo) =>
  Axios.put(`${baseUrl}/users/${id}`, updateInfo)

export const getUsersId = () => Axios.get(`${baseUrl}/users/:id`)

export const checkAccount = (accountName) =>
  Axios.get(`${baseUrl}/user/checkAccount/${accountName}`);

// students API 
export const getStudents = () => Axios.get(`${baseUrl}/students`)

export const postStudents = (students) => Axios.post(`${baseUrl}/students`, students)

export const deleteStudent = (id) => Axios.delete(`${baseUrl}/students/${id}`)

export const getStudentsId = (id) => Axios.get(`${baseUrl}/students/${id}`)

export const getStudentList = () => Axios.get(`${baseUrl}/studentList`)

export const getStudentResult = (keyword) =>
 Axios.get(`${baseUrl}/students/searchByKeyword/${keyword}`);

export const storeEnrollments = (value) => Axios
  .post(`${baseUrl}/studentEnrollment`,value);

export const getSpecificStudentEnrollment =(value)=>Axios
.get(`${baseUrl}/studentenrollment/enrols/${value}`)

export const deleteStudentEnrollment =(value)=>Axios
.delete(`${baseUrl}/studentEnrollment/${value}`)


// teachers API
export const getTeachers = (schoolId) =>
  Axios.get(`${baseUrl}/teachers?school_id=${schoolId}`)

export const postTeachers = (teachers) => Axios.post(`${baseUrl}/teachers`, teachers)

export const deleteTeachers = (id) => Axios.delete(`${baseUrl}/teachers/${id}`)

export const getTeachersId = (id) => Axios.get(`${baseUrl}/teachers/${id}`)

export const getAllTeachers = () => Axios.get(`${baseUrl}/allteachers`)

export const getTotalTeachers = () => Axios.get(`${baseUrl}/teacherList`)

export const getTeachersClass = (id) => Axios.get(`${baseUrl}/teacherallocation/${id}`)

export const addTeacherClass = (teacherclass) =>
  Axios.post(`${baseUrl}/teacherallocation`, teacherclass);

export const getTeacherList = () => Axios.get(`${baseUrl}/teacherList`);

export const getTeacherResults = (keyword) => 
Axios.get(`${baseUrl}/teachers/searchByKeyword/${keyword}`);

// class API
export const getClasses = () => Axios.get(`${baseUrl}/classes`)

export const postClasses = (classes) => Axios.post(`${baseUrl}/classes`,classes);

export const deleteClasses = (id) => Axios.delete(`${baseUrl}/classes/${id}`)

export const getEnrolledStudents = (classId) =>
  Axios.get(`${baseUrl}/filter/class/${classId}`);

export const getUnEnrolledStudents = (classId) =>
  Axios.get(`${baseUrl}/classes/${classId}/unenrolDetail`);

export const getClassDetail = (classId) =>
  Axios.get(`${baseUrl}/classesDetail/${classId}`);

export const postData = (data) => Axios.post(`${baseUrl}/postData`, data);

export const getClassesInfo = () => Axios.get(`${baseUrl}/classesInfo`);

export const getClassesByTeacher = (id) => 
  Axios.get(`${baseUrl}/class/teacher/${id}`);

export const getClassesByClassroom = (id) =>
  Axios.get(`${baseUrl}/class/classroom/${id}`);

export const getClassesBySelects = (teacherId, classroomId) => Axios.get(
    `${baseUrl}/classSelect?teacher_id=${teacherId}&classRoom_id=${classroomId}`
);

export const getClassesByKeyword = (keyword) => 
  Axios.get(`${baseUrl}/class/keyword/${keyword}`)

export const showCurrentDayClasses = () => Axios.get(`${baseUrl}/dashboard`)

export const showLessons = (classId) => Axios.get(`${baseUrl}/lessons/${classId}`);

export const postLesson = (lesson) => Axios.post(`${baseUrl}/lessons/`,lesson);

export const getLessons = () => Axios.get(`${baseUrl}/lessons`)

// school API
export const postSchools = (schoolInfo) => Axios.post(`${baseUrl}/schools`,schoolInfo)

export const showRelatedSchools = (name) => Axios.get(`/showRelatedSchools/${name}`)

// classroom API
export const getClassrooms = (schoolId) =>
  Axios.get(`${baseUrl}/classroom?school_id=${schoolId}`)

export const getAllClassrooms = () => Axios.get(`${baseUrl}/classrooms`)
  
export const getEnrolDetails = (id) => (
    Axios.get(`${baseUrl}/studentEnrollment/${id}/enrolDetail`)
  );

export const addEnrollment = (studentId,classId) => (
    Axios.post(`${baseUrl}/studentEnrollment/`, [{
      student_id: studentId,
      course_id:classId
    }]
    )
  )

export const getAge = (dob) => {
    let r = dob.match(/^(\d{1,4})(.|\/)(\d{1,2})\2(\d{1,2})$/);
    if (r === null) return false;
    r = r.map(item => item*1);
    const today = new Date(r[1], r[3] - 1, r[4]);  
    if (today.getFullYear()===r[1]&&(today.getMonth()+1)===r[3]&&today.getDate()===r[4])  
    { 
      const Y = new Date().getFullYear();  
      return (Y - r[1]);    
    }  
    return false;  
}


export const lowercaseValidation = (str) => {
  const reg = /[a-z]/g;
  const result = reg.test(str)
  return result
}


export const uppercaseValidation = (str) => {
  const reg = /[A-Z]/g;
  const result = reg.test(str)
  return result
}


export const numberValidation = (str) => {
  const reg = /[0-9]/g;
  const result = reg.test(str)
  return result
}


export const lengthValidation = (str, number) => {
  const strList = str.split('')
  return strList.length >= number;
}
  

export const emailValidation = (str) => {
  const reg = /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/;
  const result = reg.test(str)
  return result;
}


