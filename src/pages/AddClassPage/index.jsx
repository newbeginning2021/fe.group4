import React from 'react';
import AddClass from '../../components/AddClass';
import SideBar from '../../components/SideBar/SideBar';
import Header from '../../components/Header/index';
import './index.scss';
import Footer from '../../components/Footer';

function AddClassPage() {
  return (
    <div className="layout">
      <SideBar />
      <div className="addClassPage">
        <Header />
        <div className="form">
          <AddClass />
        </div>
        <Footer />
      </div>
    </div>
  );
}
export default AddClassPage;
