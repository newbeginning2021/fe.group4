import React from 'react';
import ClassProfile from '../../components/ClassProfile/index';
import './index.scss'

const ClassProfileDetailPage = (props) => {
    const { match : {params : { id } } } = props
    return (
      <ClassProfile id={id} />
    )
}
export default ClassProfileDetailPage