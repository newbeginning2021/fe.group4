import React from 'react';
import AddTeacher from '../../components/AddTeacher';
import SideBar from '../../components/SideBar/SideBar';
import Header from '../../components/Header/index';
import './index.scss';
import Footer from '../../components/Footer';

function AddTeacherPage() {
  return (
    <div className="layout">
      <SideBar />
      <div className="addTeacherPage">
        <Header />
        <div className="form">
          <AddTeacher />
        </div>
        <Footer />
      </div>
    </div>
  );
}
export default AddTeacherPage;
