import React from 'react';
import ISMSLogo from '../../components/ISMSLogo'
import RegisterForm from '../../components/RegisterForm';
import './index.scss';

function RegisterPage() {
  return (
    <div className="registerPage">
      <ISMSLogo />
      <RegisterForm />
    </div>
  )
}
export default RegisterPage;