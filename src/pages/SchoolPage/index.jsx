import React from 'react';
import './index.scss';
import SchoolForm from '../../components/SchoolForm';

function SchoolPage(props) {
  const { location: { state: { id } } } = props;
  return (
    <div className='schoolPage'>
      <h1>Welcome to  IVY</h1>
      <span>Let&apos;s set up your account in 30 seconds</span>
      <SchoolForm id={id}  />
    </div>
  )
}
export default SchoolPage;