import React from 'react';
import AddStudent from '../../components/AddStudent';
import SideBar from '../../components/SideBar/SideBar';
import Header from '../../components/Header/index';
import './index.scss';
import Footer from '../../components/Footer';

function AddStudentPage() {
  return (
    <div className="layout">
      <SideBar />
      <div className="addStudentPage">
        <Header />
        <div className="form">
          <AddStudent />
        </div>
        <Footer />
      </div>

    </div>
  );
}
export default AddStudentPage;
