import React from 'react';
import LoginForm from '../../components/LoginForm/index';
import ISMSLogo from '../../components/ISMSLogo'
import './index.scss';

function LoginPage() {
  return (
    <div className="loginPage">
      <ISMSLogo />
      <LoginForm />
    </div>
  )
}
export default LoginPage;