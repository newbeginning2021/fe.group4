const actionTypes = {
    SET_CURRENT_COUNT: 'SET_CURRENT_COUNT',
    SET_TOTAL_COUNT: 'SET_TOTAL_COUNT',
}

export default actionTypes;