import { combineReducers } from 'redux';
import user from './user/reducer';
import count from './count/reducer';
import lesson from './lesson/reducer';
import student from './student/reducer';
import classes from './class/reducer';
import teachersCount from './teachersCount/reducer';
import classCount from './classCount/reducer';
import headerCount from './headerCount/reducer';

export default combineReducers({
  user,
  count,
  lesson,
  student,
  classes,
  teachersCount,
  classCount,
  headerCount
})