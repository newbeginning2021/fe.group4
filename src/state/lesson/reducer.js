import actionType from './action-type' 

const initState = []

const lessons = (state = initState, action) => {
    const { type, data } = action
    switch (type) {
        case actionType.SET_LESSON:
            return [...data]  
        default: 
            return [...state]
    }
}
export default lessons;