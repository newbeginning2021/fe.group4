import actionType from './action-type';

export const setLessons = data => ({
    type: actionType.SET_LESSON,
    data
})
