import actionTypes from './action-type';

const initState = {
    studentCount: 0,
    teacherCount: 0,
    classCount: 0,
}

const headerCount = (state=initState, action) => {
    const { type, data } = action;
    switch (type) {
        case actionTypes.SET_CLASS_COUNT:
            return {
                ...state,
                classCount: data
            }

        case actionTypes.SET_STUDENT_COUNT:
            return {
                ...state,
                studentCount: data
            }
        
        case actionTypes.SET_TEACHER_COUNT:
            return {
                ...state, 
                teacherCount: data
            }
    
        default:
            return {
                ...state
            }
        }        
}

export default headerCount;
