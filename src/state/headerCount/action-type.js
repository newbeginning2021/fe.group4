const actionTypes = {
    SET_CLASS_COUNT: 'SET_CLASS_COUNT',
    SET_TEACHER_COUNT: 'SET_TEACHER_COUNT',
    SET_STUDENT_COUNT: 'SET_STUDENT_COUNT',
}

export default actionTypes;