import actionTypes from './action-type';

export const setClassCount = (data) => ({
    type: actionTypes.SET_CLASS_COUNT,
    data
});

export const setTeacherCount = (data) => ({
    type: actionTypes.SET_TEACHER_COUNT,
    data
})


export const setStudentCount = (data) => ({
    type: actionTypes.SET_STUDENT_COUNT,
    data
})