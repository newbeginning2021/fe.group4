import actionType from './action-type';

export const setClass = data => ({
    type: actionType.SET_CLASS,
    data
})
