import actionType from './action-type' 

const initState = []

const classes = (state = initState, action) => {
    const { type, data } = action
    switch (type) {
        case actionType.SET_CLASS:
            return [...data]  
        default: 
            return [...state]
    }
}
export default classes;