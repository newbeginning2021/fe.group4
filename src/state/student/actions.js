import actionType from './action-type';

export const setStudents = data => ({
    type: actionType.SET_STUDENT,
    data
})
