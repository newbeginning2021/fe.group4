import actionType from './action-type' 

const initState = []

const students = (state = initState, action) => {
    const { type, data } = action
    switch (type) {
        case actionType.SET_STUDENT:
            return [...data]  
        default: 
            return [...state]
    }
}
export default students;