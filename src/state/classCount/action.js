import actionTypes from './action-types';

export const setCurrentCount = (data) => ({
    type: actionTypes.SET_CURRENT_COUNT,
    data
})


export const setTotalCount = (data) => ({
    type: actionTypes.SET_TOTAL_COUNT,
    data
})