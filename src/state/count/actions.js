import actionTypes from './action-type';

export const setCurrentCount = (data) => ({
    type: actionTypes.SET_CURRENT_COUNT,
    data
})


export const setTotalCount = (data) => ({
    type: actionTypes.SET_TOTAL_COUNT,
    data
})