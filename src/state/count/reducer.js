import actionTypes from './action-type';

const initState = {
    currentCount: 0,
    totalCount: 0,
}

const count = (state=initState, action) => {
    const { type, data } = action;
    switch (type) {
        case actionTypes.SET_CURRENT_COUNT:
            return {
                ...state,
                currentCount: data
            }

        case actionTypes.SET_TOTAL_COUNT:
            return {
                ...state,
                totalCount: data
            }
            
    
        default:
            return {
                ...state
            }
        }        
}

export default count;
