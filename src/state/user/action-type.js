const actionTypes = {
    SET_USER_NAME: 'SET_USER_NAME',
    SET_USER_EMAIL: 'SET_USER_EMAIL',
    SET_USER: 'SET_USER'
}

export default actionTypes;