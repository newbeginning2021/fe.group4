import actionType from './action-type' 

const initState = {
    userName: '',
    userEmail: '',
    userInfo: ''
}

const user = (state = initState, action) => {
    const { type, data } = action
    switch (type) {
        case actionType.SET_USER_EMAIL:
            return {
                ...state,
                userEmail: data
            }   
        case actionType.SET_USER_NAME:
            return {
                ...state, 
                userName: data
            }
        case actionType.SET_USER: 
            return {
                ...state,
                userInfo: data
            }    
        default: 
            return {
                ...state
            }
    }
}

export default user;