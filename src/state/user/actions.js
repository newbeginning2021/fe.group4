import actionType from './action-type';

export const storeUserName = data => ({
    type: actionType.SET_USER_NAME,
    data
})

export const storeUserEmail = data => ({
    type: actionType.SET_USER_EMAIL,
    data
})

export const storeUserInfo = data => ({
    type: actionType.SET_USER,
    data
})