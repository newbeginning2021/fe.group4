import { Modal, Select, message } from 'antd';
import { connect } from 'react-redux';
import './index.scss';
import React from 'react';
import {withRouter} from 'react-router-dom';
import { PlusCircleFilled } from '@ant-design/icons';
import { getEnrolDetails, addEnrollment, getSpecificStudentEnrollment } from '../../api/api';
import {setClass} from "../../state/class/actions"

const { Option } = Select;
class IconButton extends React.PureComponent { 

  constructor(props){ 
    super(props);
    this.state = {
      visible: false,
      unEnrols: [],
      selected: '',
      confirmLoading: false,
    }
  }  

  showModal = () => {
    this.setState({
      visible: true
    }, () => { 
        const userInfoCache = localStorage.getItem('userInfo')
        if (userInfoCache) {
          const userInfo = JSON.parse(userInfoCache)
          getEnrolDetails(userInfo.id)
          .then(response => { 
            if (response.status === 200) { 
              this.setState({
                unEnrols:response.data.unEnrols
              })
            }
          })
         }
    });
  };
  
  handleOk = () => {
    const { selected } = this.state;
    if (selected !== '') { 
      const userInfoCache = localStorage.getItem('userInfo')
      if (userInfoCache) { 
        const userInfo = JSON.parse(userInfoCache)
        const studentId = userInfo.id;
        this.setState({
          confirmLoading:true
        }, async () => {
            const result = await addEnrollment(studentId, selected)
            if (result.status === 200) { 
              message.success({
                content: 'Successfully enrolled',
                className: 'custom-class'
              });
              const newData = await getSpecificStudentEnrollment(studentId)
              if(newData.data.length !== 0){ 
                this.props.setClass(newData.data[0].enrol_details)
              }else{
                this.props.setClass(newData.data)
              }
              this.setState({
                confirmLoading: false,
                visible: false
              })
            } else {
              message.error('please try again',5);
            } 
        })
      }
    }
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      selected:''
    });
  };

  onChange = (value) => {
    this.setState({
      selected:value
    })
  };


  render() {
    const { visible,confirmLoading,unEnrols } = this.state;
    return (
      <div className='iconButton'>
        <button type="button" onClick={this.showModal}>
          <PlusCircleFilled className='iconButton__icon'/>
          Enrol A New Class
        </button>
        <Modal
          nodeRef={this.nodeRef}
          className='modal'
          title="ENROLL STUDENT"
          visible={visible}
          onOk={this.handleOk}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancel}
          width={800}
        >
          <h1>Enrol this student in a class</h1>
          <p>Select a class and the date on which the student will be enrolled.</p>
          <div className='modal__selector'>
            <span>Select Class:</span>
            <Select
              showSearch
              allowClear
              className='modal__class'
              optionFilterProp="children"
              onChange={this.onChange}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {
                unEnrols.map(
                  (item) => <Option key={item.name} value={item.id}>{item.name}</Option>
                )
              }
            </Select>
            <span className='modal__note'>
              Only classes that the student has not enrolled are shown above
            </span>
          </div>
        </Modal>
      </div>
    )
  }
}

export default connect(
  state => ({
    newClasses:state.classes
  }),
  {
    setClass
  }
)(withRouter(IconButton));