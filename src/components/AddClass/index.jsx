import React, { useState, useEffect } from 'react';
import { Breadcrumb, notification , Form, Button, Space, Select, Input } from 'antd';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Link, useHistory } from 'react-router-dom';
import {
  postClasses,
  addTeacherClass,
  postLesson,
  getTeachers,
  getClassrooms,
} from '../../api/api';

const AddClass = () => {
  const [name, setClassName] = useState('');
  const [start_date, setStartdate] = useState('');
  const [end_date, setEnddate] = useState('');
  const [day, setDateAndTime] = useState('');
  const [teachers, setTeachers] = useState([]);
  const [teacher_id, setSelectTeacher] = useState('');
  const [classrooms, setClassrooms] = useState([]);
  const [classRoom_id, setSelectClassroom] = useState('');
  const [description, setDescription] = useState('');
  const history = useHistory();

  const openNotificationWithIcon = type=>{
    notification[type]({
      message: 'Saved Success',
      duration: 2,
    });
  }

  const resetForm = () => {
    setClassName('');
    setStartdate('');
    setEnddate('');
    setDescription('');
    setDateAndTime('');
  };

  const teacherId = teacher_id;
  const classRoomId = classRoom_id;

  const handleSubmit = async (e) => {
    e.preventDefault();
    const school_id = sessionStorage.getItem('school_id');
    const newClass = {
      name,
      start_date,
      end_date,
      day,
      teacher_id,
      classRoom_id,
      description,
      school_id,
    };
    const res = await postClasses(newClass);

    const newLessons = newClass.day.times.map((time) => ({
      date: time.date,
      start_time: time.start_time,
      end_time: time.end_time,
      teacherId,
      classRoomId,
      classId: res.data.id,
      school_id,
    }));
    await postLesson(newLessons);

    const teacherClass = {
      teacher_id,
      classId: res.data.id,
    };
    await addTeacherClass(teacherClass).then(
      () => {
       openNotificationWithIcon('success')
       history.push('/classes') 
      }
    );
  };

  const start_time = [
    { label: '08:00', value: '08:00' },
    { label: '08:30', value: '08:30' },
    { label: '09:00', value: '09:00' },
    { label: '09:30', value: '09:30' },
    { label: '10:00', value: '10:00' },
    { label: '10:30', value: '10:30' },
    { label: '11:00', value: '11:00' },
    { label: '11:30', value: '11:30' },
    { label: '12:00', value: '12:00' },
    { label: '13:30', value: '13:30' },
    { label: '14:00', value: '14:00' },
    { label: '14:30', value: '14:30' },
    { label: '15:00', value: '15:00' },
    { label: '15:30', value: '15:30' },
    { label: '16:00', value: '16:00' },
    { label: '16:30', value: '16:30' },
    { label: '17:00', value: '17:00' },
    { label: '17:30', value: '17:30' },
  ];

  const end_time = [
    { label: '08:00', value: '08:00' },
    { label: '08:30', value: '08:30' },
    { label: '09:00', value: '09:00' },
    { label: '09:30', value: '09:30' },
    { label: '10:00', value: '10:00' },
    { label: '10:30', value: '10:30' },
    { label: '11:00', value: '11:00' },
    { label: '11:30', value: '11:30' },
    { label: '12:00', value: '12:00' },
    { label: '13:30', value: '13:30' },
    { label: '14:00', value: '14:00' },
    { label: '14:30', value: '14:30' },
    { label: '15:00', value: '15:00' },
    { label: '15:30', value: '15:30' },
    { label: '16:00', value: '16:00' },
    { label: '16:30', value: '16:30' },
    { label: '17:00', value: '17:00' },
    { label: '17:30', value: '17:30' },
  ];

  const [form] = Form.useForm();

  useEffect(async () => {
    const school_id = sessionStorage.getItem('school_id');
    const res1 = await getTeachers(school_id);
    const res2 = await getClassrooms(school_id);
    setTeachers(res1.data);
    setClassrooms(res2.data);
  }, []);

  return (
    <div className="row-fluid add-page">
      <div className="span12">
        <div className="ng-pristine ng-valid" >
          <div>
            <Breadcrumb separator=">">
              <Breadcrumb.Item>
                <Link to="/classes">Classes</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item>Add Class</Breadcrumb.Item>
            </Breadcrumb>
            <div className="page__separator">
              <div className="separator__text">Enter Class Details</div>
            </div>
          </div>
          <div className="add-page__form">
            <div >
              <fieldset>
                <div className="add-page__form__header">
                  <strong>Class Details</strong>
                  <div role="button" tabIndex="0" onClick={resetForm} onKeyDown={resetForm}>
                    <FontAwesomeIcon icon={faTrash} />
                  </div>
                </div>

                <div className="control-group clearfix add-page__form__detail">
                  <div className="span6">
                    <label className="control-label" htmlFor="classname">
                      Class Name
                      <input
                        className="required-field span12"
                        value={name}
                        onChange={(e) => setClassName(e.target.value)}
                        placeholder="Class Name"
                        type="text"
                        name="Classname"
                        required="required"
                      />
                    </label>
                  </div>

                  <div className="span6">
                    <label className="control-label" htmlFor="address">
                      Class Description
                      <textarea
                        className="required-field span12"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        type="text"
                      />
                    </label>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <div className="add-page__form__header">
                  <strong>Choose teacher and classroom</strong>
                </div>
                <div className="control-group clearfix add-page__form__detail ">
                  <div className="span6">
                    <label className="control-label" htmlFor="classroom">
                      Teachers
                      <select
                        className="span12"
                        onChange={(e) => setSelectTeacher(e.target.value)}
                        required="required"
                      >
                        <option>All</option>
                        {teachers.map((element) => (
                          <option key={element.id} value={element.id}>
                            {element.name}
                          </option>
                        ))}
                      </select>
                    </label>
                  </div>

                  <div className="span6">
                    <label className="control-label" htmlFor="classroom">
                      Classrooms
                      <select
                        className="span12"
                        onChange={(e) => setSelectClassroom(e.target.value)}
                        required="required"
                      >
                        <option>All</option>
                        {classrooms.map((element) => (
                          <option key={element.id} value={element.id}>
                            {element.name}
                          </option>
                        ))}
                      </select>
                    </label>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <div className="add-page__form__header">
                  <strong>Class Schedule</strong>
                </div>
                <div className="control-group clearfix add-page__form__detail ">
                  <div className="span6">
                    <label className="control-label" htmlFor="regidate">
                      Start Date
                      <input
                        className="required-field span12"
                        valve={start_date}
                        onChange={(e) => setStartdate(e.target.value)}
                        type="date"
                        required="required"
                      />
                    </label>
                  </div>

                  <div className="span6">
                    <label className="control-label" htmlFor="enddate">
                      End Date
                      <input
                        className="required-field span12"
                        valve={end_date}
                        onChange={(e) => setEnddate(e.target.value)}
                        type="date"
                        required="required"
                      />
                    </label>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <div className="add-page__form__header">
                  <strong>Select days and times</strong>
                </div>
                <div className="control-group clearfix add-page__form__detail ">
                  <Form
                    form={form}
                    name="dynamic_form_nest_item"
                    autoComplete="off"
                    onValuesChange={(changedValues, allvalues) => setDateAndTime(allvalues)}
                  >
                    <Form.List name="times">
                      {(fields, { add, remove }) => (
                        <>
                          {fields.map((field) => (
                            <Space key={field.key} styles={{ dispaly: 'flex' }}>
                              <Form.Item
                                name={[field.name, 'date']}
                                fieldKey={[field.fieldKey, 'date']}
                                label="Date"
                              >
                                <Input type="date" />
                              </Form.Item>

                              <Form.Item
                                {...field}
                                name={[field.name, 'start_time']}
                                fieldKey={[field.fieldKey, 'start_time']}
                                label="start time"
                              >
                                <Select
                                  style={{ width: 80 }}
                                  options={start_time}
                                  value={start_time}
                                />
                              </Form.Item>

                              <Form.Item
                                {...field}
                                name={[field.name, 'end_time']}
                                fieldKey={[field.fieldKey, 'end_time']}
                                label="end time"
                              >
                                <Select style={{ width: 80 }} options={end_time} value={end_time} />
                              </Form.Item>

                              <MinusCircleOutlined onClick={() => remove(field.name)} />
                            </Space>
                          ))}

                          <Form.Item>
                            <Button
                              type="dashed"
                              onClick={() => add()}
                              block
                              icon={<PlusOutlined />}
                            >
                              Add dates and times
                            </Button>
                          </Form.Item>
                        </>
                      )}
                    </Form.List>
                  </Form>
                </div>

                <div className="button-layout">
                  <button className="cancelBtn" type="button">
                    <Link to="/classes">Cancel</Link>
                  </button>
                  <button
                    onClick={handleSubmit}
                    className="saveBtn"
                    type="submit"
                  >
                    Save
                  </button>
                </div>
              </fieldset>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default AddClass;
