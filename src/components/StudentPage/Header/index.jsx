import React from "react";
import { Link } from "react-router-dom";
import "./index.scss";

function Header() {
  return (
    <div className="students__header">
      <h1>Students</h1>
      <Link to="/addstudent">
          <button className="students__btn" type="button">
            Add Student
          </button>
        </Link>
    </div>
  );
}

export default Header;
