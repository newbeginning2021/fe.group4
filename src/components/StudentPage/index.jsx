import React from 'react';
import StudentList from './StudentList/index';
import Header from '../Header/index';
import Footer from '../Footer';
import './index.scss';
import SideBar from '../SideBar/SideBar';

const StudentPage = () => (
  <div className="layout">
    <SideBar />
    <div className='studentPage'>
      <Header />
      <div className="studentPage__container">
        <StudentList />
      </div>
      <Footer />
    </div>

    
  </div>
);

export default StudentPage;
