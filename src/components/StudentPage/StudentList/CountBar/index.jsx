import 'antd/dist/antd.css'; 
import React from 'react';

import { faUserTag } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { setCurrentCount } from '../../../../state/count/actions';
import './index.scss';

const CountBar = (props) => {
  const { currentCount, totalCount } = props;

  return (
    <div className="page__separator countBar">
      <div className="separator__text">
        <div className="countBar__container">
          <FontAwesomeIcon icon={faUserTag} className="countBar__icon" />
          <p>
            <span className="countBar__filteredResults">{currentCount}</span>
            <span className="countBar__divider">/</span>
            <span className="countBar__totalResults">{totalCount}</span>
            <span className="countBar__students">students</span>
          </p>
        </div>
      </div>
    </div>
  );
};


export default connect(
  (state) => ({
    currentCount: state.count.currentCount,
    totalCount: state.count.totalCount,
  }),
  {
    setCurrentCount,
  },
)(CountBar);
