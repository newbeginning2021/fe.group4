import React from 'react';
import { connect } from 'react-redux';
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import SearchBar from './SearchBar/index';
import SelectBar from './SelectBar/index';
import { getStudentList } from '../../../../api/api';
import { setCurrentCount, setTotalCount } from '../../../../state/count/actions';
import './index.scss';

const ListHeader = (props) => {
  const { setData, classes } = props;

  const handleSelect = async () => {
    const res = await getStudentList();
    setData(res.data);

    const input = document.querySelector('input');
    input.value = '';

    const select = document.querySelector('.ant-select-selection-item');
    select.innerHTML = 'Class';
  };

  return (
    <div className="list__header">
      <SearchBar setData={setData} />
      <SelectBar setData={setData} classes={classes} />
      <button type="button" className="list__refreshBtn" onClick={handleSelect}>
        <FontAwesomeIcon icon={faSyncAlt} />
      </button>
    </div>
  );
};

export default connect(
  (state) => ({
    currentCount: state.count.currentCount,
  }),
  {
    setCurrentCount,
    setTotalCount,
  },
)(ListHeader);
