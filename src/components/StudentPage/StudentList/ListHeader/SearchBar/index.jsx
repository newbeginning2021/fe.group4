import "antd/dist/antd.css"; 
import React, { useState } from "react";
import { connect } from "react-redux";
import { Form, Input } from "antd";
import {getStudents, getStudentResult} from '../../../../../api/api'
import {
  setCurrentCount,
  setTotalCount,
} from "../../../../../state/count/actions";
import "./index.scss";

const SearchBar = (props) => {
  const { setData } = props;
  const [keyword, setKeyword] = useState([]);

  function onChangeKeyword(e) {
    setKeyword(e.target.value);
  }

  async function getDataByKeyword() {
    let response = ""
    if(keyword === ''|| keyword instanceof(Array)) {
        response = await getStudents();
        setData(response.data);
       
    } else {
        response = await getStudentResult(keyword);
        setData(response.data);
        
    }

  }

  return (
    <Form className="searchBar" onFinish={getDataByKeyword}>
      <Form.Item name="keyword">
        <Input.Search
          value={keyword}
          placeholder="Search by name, phone or email"
          onChange={onChangeKeyword}
          onPressEnter={getDataByKeyword}
          onSearch={getDataByKeyword}
        />
      </Form.Item>
    </Form>
  );
};

export default connect(
  (state) => ({
    totalCount: state.totalCount,
  }),
  {
    setTotalCount,
    setCurrentCount,
  }
)(SearchBar)
