import "antd/dist/antd.css"; 
import React from "react";
import { connect } from 'react-redux';
import { Select } from "antd";
import {getStudents, getEnrolledStudents} from "../../../../../api/api"
import "./index.scss";
import { setCurrentCount, setTotalCount } from '../../../../../state/count/actions'


const { Option } = Select;

const SelectBar = (props) => {

  const { setData, classes } = props;

  async function handleSelect(value) {
    if (value === "") {
      const res = await getStudents();
      setData(res.data);

    } else {
      const res = await getEnrolledStudents(value);
      if (res.data instanceof Array && res.data.length !== 0) {
        const newArr = [];
        res.data.forEach((element) => newArr.push(element.students));
        setData(newArr);

      } else {
        const newArr = [];
        setData(newArr);
      }
    }
  }

  return (
    <Select
      defaultValue="Class"
      style={{ width: 120 }}
      onSelect={handleSelect}
    >
      <Option className="dropdown__list" value="">
        All
      </Option>
      {classes.map((element) => (
        <Option className="dropdown__list" key={element._id} value={element.id}>
          {element.name}
        </Option>
      ))}
    </Select>
  );
};

export default connect(
  state => (
    {
      currentCount: state.count.currentCount,
    }
  ),
  {
    setCurrentCount,
    setTotalCount
  }
)(SelectBar)