/* eslint-disable import/no-unresolved */
import 'antd/dist/antd.css'; 
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Table, Popconfirm } from 'antd';
import { Link } from 'react-router-dom';
import { faEye, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { setCurrentCount, setTotalCount } from '../../../state/count/actions';
import './index.scss';
import Avatar from './Avatar/index';
import ListHeader from './ListHeader/index';
import CountBar from './CountBar/index';
import StudentPageHeader from '../Header';
import { deleteStudent, getStudents, getClasses } from "../../../api/api";

const studentList = (props) => {
  const pageSize = 8;
  const [data, setData] = useState([]);
  const [classes, setClasses] = useState([]);
  let pageItem = '';
  let allItem = '';

  const rowSelection = {
    onChange: () => {},
  };

  async function getData() {
    const res = await getStudents();
    setData(res.data);
  }

  const handleDetele = async (record) => {
    await deleteStudent(record.id);
    const res = await getStudents();
    setData(res.data);
  };

  const columns = [
    {
      title: 'Name',
      dataIndex: 'avatar',
      render: (_, record) => (
        <Avatar url={record.img} name={record.name} dob={record.Dob} id={record.id} />
      ),
    },
    {
      title: 'Id',
      dataIndex: 'id',
    },
    {
      title: 'Phone',
      dataIndex: 'phone',
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Address',
      dataIndex: 'address',
    },
    {
      title: 'Action',
      dataIndex: 'action',
      render: (_, record, index) => (
        <div className="action">
          <Link className="action__link" to={`/students/${record.id}`}>
            <div className="action__view-container">
              <FontAwesomeIcon className="action__viewIcon" icon={faEye} />
            </div>
          </Link>
          <div className="action__delete-container">
            <Popconfirm title="Sure to delete?" onConfirm={() => handleDetele(record, index)}>
              <FontAwesomeIcon className="action__deleteIcon" icon={faTrash} />
            </Popconfirm>
          </div>
        </div>
      ),
    },
  ];

  async function getClass() {
    const res = await getClasses();
    setClasses(res.data);
  }

  const rowClassName = (record, index) => {
    let className = 'lightRow';
    if (index % 2 === 1) className = 'darkRow';
    return className;
  };

  const showTotalData = (totalData, range) => {
    allItem = totalData;
    pageItem = range[1] - range[0] + 1;
  };

  const paginationProps = {
    pageSize,
    showTotal: showTotalData,
  };

  useEffect(() => {
    getData();
    getClass();
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      if (!allItem) {
        props.setTotalCount(0);
        props.setCurrentCount(0);
      } else {
        props.setCurrentCount(pageItem);
        props.setTotalCount(allItem);
      }
    });
    return () => clearInterval(interval);
  });

  return (
    <div className="studentPage__container">
      <StudentPageHeader />
      <div className="student__list">
        <CountBar />
        <div className="student__table">
          <ListHeader setData={setData} classes={classes} />

          <Table
            pagination={paginationProps}
            rowKey={(record) => record._id}
            rowSelection={{ ...rowSelection }}
            columns={columns}
            dataSource={data}
            rowClassName={rowClassName}
          />
        </div>
      </div>
    </div>
  );
};

export default connect(
  (state) => ({
    currentCount: state.count.currentCount,
  }),
  {
    setCurrentCount,
    setTotalCount,
  },
)(studentList);
