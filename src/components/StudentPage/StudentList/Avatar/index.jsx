
import React from 'react';
import { Link } from 'react-router-dom';
import './index.scss'

function Avatar(props) {
    const { url, name, dob, id } = props;    
    return (
      <div className='name__container'>
        <img className='name__avatar' alt='avatar' src={url} />
        <div className='name__description'>
          <Link to={`/students/${id}`}>{name}</Link>
          <p><span>{dob}</span></p>
        </div> 
      </div>
    )
}
export default Avatar;