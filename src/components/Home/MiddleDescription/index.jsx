import React from 'react';
import './index.scss';

function MiddleDescription() {
    return (
        <div className='section__container'>
            <div className="section__title">
                <h2>Manage Your Entire Teaching Business</h2>
                <h4>
                  Ivy school management system combines everything you need
                  to run your teaching business.
                  <br/>
                  Its so simple and intuitive that you wont need training to master it.
                </h4>
            </div>
        </div>
    )
}

export default MiddleDescription
