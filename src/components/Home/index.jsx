import React from 'react';
import Banner from './Banner/index';
import Nav from './Nav/index';
import MiddleDescription from './MiddleDescription';
import Features from './Features';
import Dashboard from '../../pages/Dashboard';
import { isAuthenticated } from '../../service/auth';
import './index.scss';
import Footer from '../Footer/index'
import DemoBanner from './DemoBanner';

function Home() {
  return (
    <div>
      {isAuthenticated() ? (
        <Dashboard />
      ) : (
        <div className="home">
          <Nav />
          <div className="section__container">
            <Banner />
          </div>
          <DemoBanner />
          
          <MiddleDescription />
          <Features />
         
          <Footer black='black' />
        </div>
      )}
    </div>
  );
}

export default Home;
