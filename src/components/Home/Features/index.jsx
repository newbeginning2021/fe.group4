import React from "react";
import { Row } from 'antd';
import TestBlock from './TestBlock';
import data from '../../../img/TnG_data.png';
import device from '../../../img/TnG_devices.png';
import increase from '../../../img/TnG_increase.png';
import schedule from '../../../img/TnG_schedule.png';
import support from "../../../img/TnG_support.png"
import time from '../../../img/TnG_time.png';
import "./index.scss";

function Features() {
  const content1=`From students’ attendance to invoicing, 
  everything is kept in one place, accessible anywhere and any time.`
  const content2=`Tasks which take 5-10 minutes on paper or excel, 
  only take a few seconds on Teach 'n Go, saving you loads of time.`
  const content3=`Our student/parent portal and automated emails/SMS, 
  give you a professional edge which keeps people coming back.`
  const content4=`Whenever you need help we'll be there, and we will 
  do whatever it takes to make sure you get your job done.`
  const content5=`Rest assured that our robust servers will store your 
  data safely and securely in accordance with GDPR regulations.`
  const content6=`Because Teach 'n Go is based on the cloud, you can 
  access it anytime, anywhere using any device that has an internet connection.`
  return (
    <div className="section__container features">
      <div className="section__feature">
        <Row>
          <TestBlock image={schedule} head='Tracking Everything' content={content1}/>
          <TestBlock image={time} head='Save Time' content={content2}/>
          <TestBlock image={increase} head='Increase Bookings' content={content3}/>
        </Row>
        <Row>
          <TestBlock image={support} head='First-Class Support' content={content4}/>
          <TestBlock image={data} head='Secure Data' content={content5}/>
          <TestBlock image={device} head='Access Anytime, Anywhere' content={content6}/>
        </Row>
      </div>
    </div>
  );
}

export default Features;
