import React from "react";
import { Col } from "antd";
import "./index.scss";

function TestBlock(props) {
  const { image ,head,content} = props
  return (
    <Col xs={24} sm={24} md={8} lg={8} xl={8} className="feature">
      <div className="feature__test-block">
        <img src={image} alt="" />
        <div className="feature__caption">
          <h3>{head}</h3>
          <p>
            {content}
          </p>
        </div>
      </div>
    </Col>
  );
}

export default TestBlock;
