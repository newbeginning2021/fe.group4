import React from 'react';
import { Link } from 'react-router-dom';
import hero from '../../../img/hero.jpeg';
import { isAuthenticated } from '../../../service/auth';
import './index.scss';

function Banner() {
  return (
    <div className="banner">
      <div className="banner__left">
        <h1 className="banner__title">School Management Software Simplified</h1>
        <div className="banner__content">
          <h4>Everything you need to manage your training centre</h4>
        </div>
        <div className="banner__button">
          {isAuthenticated() ? (
            <Link to="/dashboard">Dashboard</Link>
          ) : (
            <Link to="/register"> TRY IT NOW FOR FREE</Link>
          )}
        </div>
      </div>
      <div className="banner__right">
        <img src={hero} alt="hero" />
      </div>
    </div>
  );
}

export default Banner;
