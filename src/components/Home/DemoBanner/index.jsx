import React from 'react';
import dashboardPic from '../../../img/dashboard.jpg';
import tablePic from '../../../img/table.png';
import './index.scss';

function DemoBanner() {
  return (
    <div className="demoBanner">
      <div className="demoBanner__left">
        <h2>
          Improve your Teaching <br /> Business
        </h2>
        <p>
          Now you can spend more time actually doing <br />
          what your want to do... teaching!
        </p>
      </div>
      <div className="demoBanner__right">
        <img src={dashboardPic} alt="websiteDemo"/>
        <img className='demoBanner__tableImg' src={tablePic} alt="websiteDemo"/>
      </div>
    </div>
  );
}

export default DemoBanner;
