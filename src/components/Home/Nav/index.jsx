import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../../img/white.svg';
import './index.scss';

function Nav() {
  const [show, setShow] = useState(false);

  function handleShow() {
    window.addEventListener('scroll', () => {
      if (window.scrollY > 1) {
        setShow(true);
      } else {
        setShow(false);
      }
    });
  }

  useEffect(() => {
    handleShow();
  }, []);

  return (
    <div className={`nav ${show && 'nav__white'}`}>
      <div className="section__container nav__container">
        <div className="nav__left">
          <div className="logo">
            <img src={Logo} alt="logo-nav" />
          </div>
          <h4>Ivy School Management System</h4>
        </div>
        <div className="nav__buttons">
          <div className="nav__loginButton">
            <Link to="/login">LOG IN</Link>
          </div>
          <div className="nav__tryButton">
            <a href="/register">TRY IT FREE</a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Nav;
