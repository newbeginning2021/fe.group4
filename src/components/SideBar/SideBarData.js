import React from 'react'
import { faHome, faUserFriends,
        faChalkboardTeacher, faBook, faCalendar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const SidebarData = [
    {
        title: "Dashboard",
        link: '/dashboard',
        icon: <FontAwesomeIcon icon={faHome} />,
        index: 0
    },
    {
        title: "Calendar",
        link: '/calendar',
        icon: <FontAwesomeIcon icon={faCalendar} />,
        index: 1
    },

    {
        title: "Students",
        link: '/students',
        icon: <FontAwesomeIcon icon={faUserFriends} />,
        index: 2
    },

    {
        title: "Teachers",
        link: '/teachers',
        icon: <FontAwesomeIcon icon={faChalkboardTeacher} />,
        index: 3
    },

    {
        title: "Classes",
        link: '/classes',
        icon: <FontAwesomeIcon icon={faBook} />,
        index: 4
    },
]

export default SidebarData