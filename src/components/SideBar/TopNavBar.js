import React from "react";
import "./TopNavBar.scss";
import { Link } from "react-router-dom";
import sidebarData from "./SideBarData";

function TopNavBar({ displayStyle }) {
  return (
    <>
      <div className="TopNavBar__container" style={displayStyle}>
        <ul>
          {sidebarData.map((item) => (
            <div 
              key={item.index}
              className={
                item.link === window.location.pathname
                  ? "TopNavBar-item-wrapper link-active"
                  : "TopNavBar-item-wrapper"
              }
            >
              <Link
                key={sidebarData.index}
                style={{ textDecoration: "none" }}
                to={item.link}
                onClick={(event) => {
                }}
              >
                <li
                  
                  className="TopNavBar-item"
                >
                  <span className="TopNavBar-item__icon">{item.icon}</span>
                  <span className="TopNavBar-item__title">{item.title}</span>
                </li>
              </Link>
            </div>
          ))}
        </ul>
      </div>
    </>
  );
}

export default TopNavBar;
