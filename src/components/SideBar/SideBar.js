import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import Logo from '../../img/white.svg';
import sidebarData from './SideBarData';
import './SideBar.scss';


function SideBar({ displayStyle }) {
  return (
    <>
      <div className="sideBar__container" style={displayStyle}>
        <Link to='/' className="sideBar__brand">
          <span className="sideBar__avatarContainer">
            <span className="sideBar__avatar">
              <img src={Logo} alt="luma logo" />
            </span>
          </span>
          <span>IVY</span>
        </Link>
        <div className="sideBar__heading">Instructor</div>
        <ul>
          {sidebarData.map((item) => (
            <li
              key={item.index}
              aria-hidden="true"
            >
              <NavLink 
              style={{ textDecoration: 'none' }} 
              to={item.link} 
              activeClassName="active"
              className="sideBar__button"
              >
                <span className="sideBar__icon">{item.icon}</span>
                <span className="sideBar__title">{item.title}</span>
              </NavLink>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}

export default SideBar;
