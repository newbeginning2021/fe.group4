import React, { useState, useRef } from 'react';
import { AutoComplete } from 'antd';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { storeUserName, storeUserEmail } from '../../state/user/actions'
import { showRelatedSchools, postSchools, putUsers } from '../../api/api'
import './index.scss';

const { Option } = AutoComplete;

function SchoolForm(props) {
  
  const [name, setName] = useState('')
  const [school, setSchool] = useState('')
  const [schoolId, setSchoolId] = useState('')
  const [schoolList, setSchoolList] = useState([])
  const [addressDisabled, setAddressDisabled] = useState(false)
  
  const addressInput = useRef()
  const error = useRef()

  const handleName = (event) => {
    if (name !== '' && school !== '' && addressInput.current.value !== '') { 
      error.current.classList.remove('show')
    }
    const currentName = event.target.value;
    setName(currentName)
  }

  const handleSearch = async searchValue => {
    if (searchValue !== '') {
      if (name !== '' && addressInput.current.value !== '') { 
        error.current.classList.remove('show')
      }
      const searchResult = await showRelatedSchools(searchValue);
      const searchList = searchResult.data
      if (searchList.length !== 0) {
        for (let i = 0; i < searchList.length; i += 1) {
          if (searchValue.toLowerCase() === searchList[i].name.toLowerCase()) {
            addressInput.current.value = searchList[i].address
            addressInput.current.classList.add('disabled')
            addressInput.current.disabled = 'disabled'
            setSchoolId(searchList[i].id)
            setAddressDisabled(true)
            break;
          } else {
            setAddressDisabled(false)
            addressInput.current.classList.remove('disabled')
            addressInput.current.disabled = ''
          }
        }
      } else { 
        setAddressDisabled(false)
        addressInput.current.classList.remove('disabled')
        addressInput.current.disabled = ''
      }
      setSchoolList(searchList)
      setSchool(searchValue)
    } else {
      setSchoolList([]);
      setSchool('')
      setAddressDisabled(false)
    }
  };

  const handleSelect = async (value, option) => {
    addressInput.current.value = option.address
    addressInput.current.classList.add('disabled')
    addressInput.current.disabled='disabled'
    setSchool(option.children)
    setSchoolId(option.id)
    setAddressDisabled(true)
  };

  const handleAddress = () => {
    if (name !== '' && school !== '' && addressInput.current.value !== '') { 
      error.current.classList.remove('show')
    }
  }

  const handleSave = async () => {
    if (name !== '' && school !== '' && addressInput.current.value !== '') {
      error.current.classList.remove('show')
      if (addressDisabled === true) {
        const result2 = await putUsers(props.id, {
          name,
          school_id:schoolId
        })
        sessionStorage.setItem('school_id', result2.data[0].school_id)
      } else { 
        const result1 = await postSchools({
          name: school,
          address:addressInput.current.value
        })
        const result2 = await putUsers(props.id, {
          name,
          school_id:result1.data.id
        })
        sessionStorage.setItem('school_id',  result2.data[0].school_id)
      }
      sessionStorage.setItem('username', name)
      props.history.push('./dashboard')
    } else { 
      error.current.classList.add('show')
    }

    const userEmail = sessionStorage.getItem('email')
    props.storeUserEmail(userEmail)

    props.storeUserName(name)
  }

  return (
    <div className='schoolForm'>
      <form>
        <p>What&apos;s your name?</p>
        <input type="text" onChange={handleName}/>
        <p>What&apos;s the name of your school?</p>
        <AutoComplete
          showSearch
          labelInValue
          defaultActiveFirstOption={false}
          showArrow={false}
          filterOption
          onSearch={handleSearch}
          onSelect={handleSelect}
          notFoundContent={null}
        >
          {
            schoolList.map(d => <Option
              key={d.name} id={d.id} address={d.address}>{d.name}
            </Option>)
          }
      </AutoComplete>
        <p>What&apos;s the address of your school?</p>
        <input type="text" ref={addressInput} onChange={handleAddress}/>
        <small ref={error}>you have required field unfinished</small>
        <button type='button' onClick={handleSave}>Save</button>
      </form>
    </div>
  )
}

export default withRouter(connect(
  state => ({
    userName: state.user.userName
  }),
  {
    storeUserName,
    storeUserEmail
  } 
  )(SchoolForm));
