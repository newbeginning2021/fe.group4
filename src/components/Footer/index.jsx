import React from 'react';
import logo from '../../img/black-70@2x.png';
import './index.scss'

function Footer(props) {
  const { black } = props
  return (
    <div className={`footer ${black}`}>
      <div className='footer__container'>
        <p className='footer__logo'>
          <img src={logo} alt="logo" />
          IVY
        </p>
        <p className='footer__description'>
          IVY is the user interface for modern Education Platforms, helps 
          educational institutions by automating regular administrative tasks, which offers 
          a range of functionality for everything from students, teachers admissions to classes
          scheduling and related affairs, including classes dashboard, calendar, curriculum 
          management and more.
        </p>

        <p>
          <span>Terms</span>
          <span>Policy</span>
        </p>
        <p>Copyright 2021 © All rights reserved.</p>
      </div>
    </div>
  );
}

export default Footer;
