import 'antd/dist/antd.css'; 
import React, { useState } from 'react';
import { Form, Input } from 'antd';
import { getClassesInfo, getClassesByKeyword } from '../../../../../api/api';
import './index.scss'

const SearchBar = (props) => {
    const { setData } = props
    const [keyword, setKeyword] = useState([]);
    function onChangeKeyword(e) {
        setKeyword(e.target.value) 
    }
    async function getDataByKeyword() {
        let res = ''
        if(keyword === '') {
          res = await getClassesInfo();
        } else{
          res = await getClassesByKeyword(keyword);
        }
        setData(res.data) 
    } 
    return (
      <Form className='searchBar' onFinish={getDataByKeyword}>
        <Form.Item name="keyword">
          <Input.Search
            value={keyword}
            placeholder="Search by title"
            onChange={onChangeKeyword}
            onPressEnter={getDataByKeyword}
            onSearch={getDataByKeyword}
          />
        </Form.Item>
      </Form>
    )
}

export default SearchBar