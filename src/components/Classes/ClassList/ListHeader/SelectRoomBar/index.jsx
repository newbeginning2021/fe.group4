import 'antd/dist/antd.css'; 
import React from 'react';
import { Select } from 'antd';
import { getClassesInfo, getClassesByTeacher, 
  getClassesByClassroom, getClassesBySelects } from '../../../../../api/api';

const { Option } = Select;
const SelectRoomBar = (props) => {
  const { setData, teacherId, setClassroomId, classrooms } = props; 
  
  async function handleSelect(value) {
    setClassroomId(value);
    let res = [];
    if (value === ''){
      if(teacherId === '') {
        res = await getClassesInfo();
      } else {
        res = await getClassesByTeacher(teacherId);
      }
    } else if(teacherId === ''){
        res = await getClassesByClassroom(value);
      } else {
        res = await getClassesBySelects(teacherId, value);
    }
    setData(res.data);
  }
  return (
    <Select
      className='list__header--dropdown'
      defaultValue='Classroom'
      onSelect={handleSelect}
    >
      <Option className='list__option' value=''>All</Option>
      {
        classrooms.map(element => (
          <Option className='list__option' key={element.id} value={element.id}>
            {element.name}
          </Option>
        ))
      }
    </Select>
  )
}

export default SelectRoomBar;