import 'antd/dist/antd.css'; 
import React from 'react';
import { Select } from 'antd';
import { getClassesInfo, getClassesByTeacher, 
  getClassesByClassroom, getClassesBySelects } from '../../../../../api/api';
import './index.scss';

const { Option } = Select;
const SelectTeacherBar = (props) => {
  const { setData, teachers, classroomId, setTeacherId } = props;
  async function handleSelect(value) {
    setTeacherId(value);
    let res = [];
    if (value === ''){
      if(classroomId === '') {
        res = await getClassesInfo();
      } else {
        res = await getClassesByClassroom(classroomId);
      }
    } else if(classroomId === ''){
        res = await getClassesByTeacher(value);
      } else {
        res = await getClassesBySelects(value, classroomId);
    }
    setData(res.data);
  }

  return (
    <Select
      className='list__header--dropdown'
      defaultValue='Teachers'
      style={{ width: 120 }}
      onSelect={handleSelect}
    >
      <Option className='list__option' value=''>All</Option>
      {
        teachers.map(element => (
          <Option className='list__option' key={element.id} value={element.id}>
            {element.name}
          </Option>
        ))
      }
    </Select>
  )
}

export default SelectTeacherBar;