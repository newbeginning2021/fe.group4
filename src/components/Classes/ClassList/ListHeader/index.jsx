import React from 'react';
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {getClassesInfo} from '../../../../api/api';
import SearchBar from './SearchBar/index';
import SelectTeacherBar from './SelectTeacherBar/index';
import SelectRoomBar from './SelectRoomBar/index';

const ListHeader = (props) => {
  const {
    setData,
    teachers,
    classrooms,
    teacherId,
    setTeacherId,
    classroomId,
    setClassroomId,
  } = props;

  const handleSelect = async () => {
    const res = await getClassesInfo();
    setData(res.data);
    setClassroomId('');
    setTeacherId('');

    const input = document.querySelector('input');
    input.value = '';

    const selectTeacher = document.querySelector('.teacherBar .ant-select-selection-item');
    selectTeacher.innerHTML = 'Teacher';

    const selectRoom = document.querySelector('.roomBar .ant-select-selection-item');
    selectRoom.innerHTML = 'Classroom';
  };
  return (
    <div className="list__header">
      <SearchBar setData={setData} />
      <div className="list__selector">
        <div className="list__teacherBar">
          <SelectTeacherBar
            setData={setData}
            teachers={teachers}
            classroomId={classroomId}
            setTeacherId={setTeacherId}
          />
        </div>
        <div>
          <SelectRoomBar
            setData={setData}
            classrooms={classrooms}
            teacherId={teacherId}
            setClassroomId={setClassroomId}
          />
        </div>
      </div>
      <button type="button" className="list__refreshBtn" onClick={handleSelect}>
        <FontAwesomeIcon icon={faSyncAlt} />
      </button>
    </div>
  );
};

export default ListHeader;
