import 'antd/dist/antd.css'; 
import React from 'react';
import { connect } from 'react-redux';
import { faChalkboard } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './index.scss';

const CountBar = (props) => {
  const { currentCount, totalCount } = props;
  return (
    <div className="page__separator countBar__separator">
      <div className="separator__text">
        <div className="countBar__container">
          <FontAwesomeIcon icon={faChalkboard} className="countBar__icon" />
          <p>
            <span className="countBar__filteredResults">{currentCount}</span>
            <span className="countBar__divider">/</span>
            <span className="countBar__totalResults">{totalCount}</span>
            <span className="countBar__classes">classes</span>
          </p>
        </div>
      </div>
    </div>
  );
};

export default connect((state) => ({
  currentCount: state.classCount.currentCount,
  totalCount: state.classCount.totalCount,
}))(CountBar);
