import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Table, Popconfirm } from 'antd';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css'; 
import { faEye, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getClassesInfo, deleteClasses, getAllTeachers, getAllClassrooms } from '../../../api/api';
import { setCurrentCount, setTotalCount } from '../../../state/classCount/action';
import ListHeader from './ListHeader/index';
import CountBar from './CountBar/index';
import './index.scss'

const ClassList = (props) => {
  const pageSize = 8;
  const [data, setData] = useState([]);
  const [teachers, setTeachers] = useState([]);
  const [classrooms, setClassrooms] = useState([]);
  const [teacherId, setTeacherId] = useState('');
  const [classroomId, setClassroomId] = useState('');
  let pageItem = '';
  let allItem = '';


  const handleDetele = async (record) => {
    await deleteClasses(record.id)
    const res = await getClassesInfo();
    setData(res.data);
  };
  const columns = [
    {
      title: 'Title',
      dataIndex: 'name',
      render: (_, record) => (
        <Link className="class__title" to={`/classes/${record.id}`}>
          {record.name}
        </Link>
      ),
    },
    {
      title: 'Teacher',
      dataIndex: 'teachers',
      render: (_, record) => record.teachers[0].name,
    },
    {
      title: 'Classroom',
      dataIndex: 'classrooms',
      render: (_, record) => record.classrooms[0].name,
    },
    {
      title: 'Start Date',
      dataIndex: 'start_date',
      render: (start_date) => start_date.split('T')[0],
      sorter: (a, b) => a.start_date.split('-')[1] - b.start_date.split('-')[1] === 0 ? 
      a.start_date.split('T')[0].split('-')[2] - b.start_date.split('T')[0].split('-')[2] :
       a.end_date.split('T')[0].split('-')[1] - b.end_date.split('T')[0].split('-')[1],
      sortDirections: ['descend', 'ascend', 'descend'],
    },
    {
      title: 'End Date',
      dataIndex: 'end_date',
      render: (end_date) => end_date.split('T')[0],
      sorter: (a, b) => a.end_date.split('T')[0].split('-')[1] - 
      b.end_date.split('T')[0].split('-')[1] === 0 ?
      a.end_date.split('T')[0].split('-')[2] - b.end_date.split('T')[0].split('-')[2] : 
      a.end_date.split('T')[0].split('-')[1] - b.end_date.split('T')[0].split('-')[1],
      sortDirections: ['descend', 'ascend', 'descend'],
    },
    {
      title: 'Action',
      dataIndex: 'action',
      render: (_, record, index) => (
        <div className="action">
          <Link className="action__viewProfile" to={`/classes/${record.id}`}>
            <div className="action__viewIcon-container">
              <FontAwesomeIcon className="action__viewIcon" icon={faEye} />
            </div>
          </Link>

          <div className="action__deleteIcon-container">
            <Popconfirm title="Sure to delete?" onConfirm={() => handleDetele(record, index)}>
              <FontAwesomeIcon className="action__deleteIcon" icon={faTrash} />
            </Popconfirm>
          </div>
        </div>
      ),
    },
  ];

 
  async function getData() {
    const res = await getClassesInfo();
    setData(res.data);
  }


  async function getTeachers() {
    const res = await getAllTeachers();
    
    setTeachers(res.data);
  }
  async function getClassrooms() {
    const res = await getAllClassrooms();
    setClassrooms(res.data);
  }

  const rowClassName = (record, index) => {
    let className = 'lightRow';
    if (index % 2 === 1) className = 'darkRow';
    return className;
  };

  const showTotalData = (totalData, range) => {
    pageItem = range[1] - range[0] + 1;
    allItem = totalData;
  };

  const paginationProps = {
    pageSize,
    showTotal: showTotalData,
  };
  useEffect(() => {
    getData();
    getTeachers();
    getClassrooms();
  }, []);

  useEffect(() => {
    const interval = setInterval(()=>{
      if (!allItem) {
        props.setCurrentCount(0);
        props.setTotalCount(0);
      } else {
        props.setCurrentCount(pageItem);
        props.setTotalCount(allItem);
      }
    })
    return () => clearInterval(interval)
  });

  return (
    <div className="class__list">
      <CountBar />
      <div className="class__table">
        <ListHeader
          setData={setData}
          teachers={teachers}
          classrooms={classrooms}
          teacherId={teacherId}
          setTeacherId={setTeacherId}
          classroomId={classroomId}
          setClassroomId={setClassroomId}
        />
        <Table
          pagination={paginationProps}
          rowKey={(record) => record.id}
          columns={columns}
          dataSource={data}
          rowClassName={rowClassName}
        />
      </div>
    </div>
  );
};

export default connect(
  state => ({
    totalCount: state.classCount.totalCount,
  }),
  {
    setCurrentCount,
    setTotalCount
  }
)(ClassList);
