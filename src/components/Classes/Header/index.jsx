import React from 'react';
import { Link } from 'react-router-dom';
import './index.scss';

function Header() {
  return (
    <div className="class-page__header">
      <h1>Classes</h1>
      <div>
        <Link to="/addclass">
          <button className="class-page__btn" type="button">
            Add Class
          </button>
        </Link>
      </div>
    </div>
  );
}

export default Header;
