import React from 'react';
import ClassesHeader from './Header/index';
import ClassList from './ClassList/index';
import Header from '../Header/index';
import Footer from '../Footer';
import './index.scss';
import SideBar from '../SideBar/SideBar';

const Classes = () => (
  <div className="layout">
    <SideBar />
    <div>
      <Header />
      <div className="classPage__container">
        <ClassesHeader />
        <ClassList />
      </div>
      <Footer />
    </div>
    
  </div>
);

export default Classes;
