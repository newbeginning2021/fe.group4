import React, { useEffect, useState } from 'react';
import { Calendar } from 'antd';
import moment from 'moment';
import { getLessons } from '../../api/api';
import Header from '../Header';
import SideBar from '../SideBar/SideBar';
import Footer from '../Footer';
import './index.scss';

function CalendarComponent() {
  const [lessons, setLessons] = useState([]);

  const onSelect = (value) => {
    moment(value).format('YY-MM-DD');
  };

  useEffect(async () => {
    let mounted = true;
    const res = await getLessons();
    if (mounted) { 
      setLessons(res.data);
    }
    return () => { 
      mounted = false;
    }
  }, []);

  function dateFullCellRender(value) {
    let listData = [];
    lessons.forEach((lesson) => {
      const day = moment(lesson.date).format('YYYY-MM-DD');

      if (day === value.format('YYYY-MM-DD')) {
        listData = [
          ...listData,
          {
            type: 'warning',
            content: [
              lesson.classId, 
              lesson.start_time, 
              lesson.end_time, 
              lesson.classInfo[0]?.name, 
              lesson.studentInfo?.length, 
              lesson.classroomInfo[0]?.name, 
              lesson._id, 
            ],
            id: lesson.id,
          },
        ];
      }
    });

    return (
      <ul className="events">
        {listData.map((item) => (
          <div key={item.content[6]} className="events__container">
            <ul className="events__content">
              <li className="events__time">
                {item.content[1]} - {item.content[2]}
              </li>
              <li>{item.content[3]}</li>
            </ul>
          </div>
        ))}
      </ul>
    );
  }

  return (
    <div className="layout">
      <SideBar />
      <div>
        <Header />
        <section className="page__container calendar__container">
          <h1>Calendar</h1>
          {lessons[0] ? (
            <Calendar
              dropdownClassName="calendar__dropdown"
              dateCellRender={dateFullCellRender}
              onChange={onSelect}
            />
          ) : null}
        </section>
        <Footer />
      </div>
    </div>
  );
}

export default CalendarComponent;
