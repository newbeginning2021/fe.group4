import { connect } from 'react-redux';
import { Modal, DatePicker, Select, message } from 'antd';
import React, { useState, useEffect } from 'react';
import { PlusCircleFilled } from '@ant-design/icons';
import moment from 'moment';
import { getTeachers, getClassrooms, postLesson, showLessons } from '../../../api/api';
import './index.scss';
import { setLessons } from '../../../state/lesson/actions';

const children = [];
const { Option } = Select;

const teacherList = [];

for (let i = 0; i < 48; i += 1) {
  const hour = Math.floor(i / 2);
  const modifiedHour = hour > 9 ? hour.toString() : `0${hour}`;
  const minute = i % 2;
  const modifiedMinute = minute === 1 ? '30' : '00';
  const result = `${modifiedHour}:${modifiedMinute}`;
  children.push(result);
}

for (let i = 0; i < 3; i += 1) {
  teacherList.push(<Option key={i.toString(36) + i}>Mark</Option>);
}
const dateFormat = 'YYYY-MM-DD';

const IconButton = (props) => {
  const datepicker = React.useRef();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [dateTime, setDateTime] = useState(moment().format('YYYY-MM-DD'));
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');
  const [teacher, setTeacher] = useState([]);
  const [classroom, setClassroom] = useState([]);
  const [selectedTeacher, setSelectedTeacher] = useState('');
  const [selectedClassroom, setSelectedClassroom] = useState('');

  useEffect(() => {}, []);

  const showModal = () => {
    const { schoolId } = props;
    getClassrooms(schoolId).then((response) => {
      if (response.status === 200) {
        setClassroom(response.data);
      } else {
        setIsModalVisible(false);
        message.error('Internet error!Please try again', 5);
      }
    });
    getTeachers(schoolId).then((response) => {
      if (response.status === 200) {
        setTeacher(response.data);
      } else {
        setIsModalVisible(false);
        message.error('Internet error!Please try again', 5);
      }
    });

    setIsModalVisible(true);
  };

  const handleOk = () => {
    const lesson = [
      {
        date: dateTime,
        start_time: startTime,
        end_time: endTime,
        classId: props.classId,
        teacherId: selectedTeacher,
        classRoomId: selectedClassroom,
      },
    ];
    postLesson(lesson)
      .then(async (response) => {
        if (response.status === 200) {
          setIsModalVisible(false);
          message.success(
            {
              content: 'Saved success!',
              className: 'custom-class',
            },
            5,
          );
          const newData = await showLessons(props.classId);
          props.setLessons(newData.data);
        } else {
          message.error(
            {
              content: 'Internet error! Please try again',
              className: 'custom-class',
            },
            5,
          );
        }
      })
      .catch(() => {
        message.error('Catch error! Please try again', 5);
      });
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleDate = (_, dateString) => {
    setDateTime(dateString);
  };

  const handleStartTime = (value) => {
    setStartTime(value);
  };

  const handleEndTime = (value) => {
    setEndTime(value);
  };

  const handleTeacher = (value) => {
    setSelectedTeacher(value);
  };

  const handleClassRoom = (value) => {
    setSelectedClassroom(value);
  };

  return (
    <div className="iconButton">
      <button type="button" onClick={showModal}>
        <PlusCircleFilled className="iconButton__icon" />
        Add a Lesson
      </button>
      <Modal
        title="ADD A NEW LESSON"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        className="lessonForm"
        width={800}
      >
        <h4>Add a new lesson</h4>
        <p>Select the options below to add a new lesson</p>
        <p>Select the lesson date and time</p>
        <div className="lessonForm__timeContainer">
          <DatePicker
            ref={datepicker}
            className="lessonForm__date"
            defaultValue={moment(moment().format('YYYY-MM-DD'), dateFormat)}
            format={dateFormat}
            onChange={handleDate}
          />
          <Select
            showSearch
            className="lessonForm__time"
            size="large"
            placeholder="start time"
            onChange={handleStartTime}
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {children.map((item) => (
              <Option key={item}>{item}</Option>
            ))}
          </Select>
          <Select
            className="lessonForm__time"
            showSearch
            size="large"
            placeholder="end time"
            onChange={handleEndTime}
          >
            {children.map((item) => (
              <Option key={item}>{item}</Option>
            ))}
          </Select>
        </div>
        <div className="lessonForm__teacherClassroom">
          <div className="lessonForm__teacherContainer">
            <p>Teacher</p>
            <Select
              showSearch
              className="lessonForm__teacher"
              size="large"
              placeholder="teacher"
              onChange={handleTeacher}
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {teacher.map((item) => (
                <Option key={item.id}>{item.name}</Option>
              ))}
            </Select>
          </div>
          <div className="lessonForm__classroomContainer">
            <p>Classroom</p>
            <Select
              showSearch
              className="lessonForm__teacher"
              size="large"
              placeholder="classroom"
              onChange={handleClassRoom}
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {classroom.map((item) => (
                <Option key={item.id}>{item.name}</Option>
              ))}
            </Select>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default connect(
  (state) => ({
    lessons: state.lesson,
  }),
  {
    setLessons,
  },
)(IconButton);
