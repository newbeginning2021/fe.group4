import { connect } from 'react-redux';
import 'antd/dist/antd.css'; 
import React, {useEffect } from 'react';
import { Table, Popconfirm,message} from 'antd';
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './index.scss';
import {getEnrolledStudents, deleteStudentEnrollment} from '../../../api/api';
import Avatar from '../../StudentPage/StudentList/Avatar';
import {setStudents} from '../../../state/student/actions'

const Form = (props) => {
  const pageSize = 8
  const { classId, newStudents } = props;
  

  async function getData() {
    getEnrolledStudents(classId)
      .then(response => {
        if (response.status === 200) { 
          props.setStudents(response.data)
        }
      })
  }

  useEffect(() => {
    getData()
  },[])
      
  const handleDelete = async (record) => {
    const res = await deleteStudentEnrollment(record.id)
    if (res.status === 201) {
      message.success({
        content: 'Delete successfully!',
        className:'custom-class'
      }, 5)
      getEnrolledStudents(classId).then(response => {
        if (response.status === 200) { 
          props.setStudents(response.data)
        }
      })
    } else { 
      message.error({
        content: 'Internet error! Please try again',
        className:'custom-class'
      }, 5)
    }
    }

    const columns = [
      {
          title: 'Name',
          dataIndex: 'avatar',
          render: (_, record) => 
            <Avatar
              url={record.students.img}
              name={record.students.name}
              dob={record.students.Dob}
              id={record.id}
            />
      },
      {
          title: 'Phone',
          dataIndex: ['students','phone']
      },
      {
          title: 'Email',
          dataIndex: ['students','email'],
      },
      {
          title: 'Action',
          dataIndex: 'action',
          render: (_, record) => (
            <div className='action'>
              <div className="deleteIcon__container">
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => handleDelete(record)}
              >
                <FontAwesomeIcon
                  className='deleteIcon'
                  icon={faTrash}
                  title='Remove'
                />
              </Popconfirm>
              </div>
            </div>
          )
      }
  ]  
    const paginationProps = {
      pageSize,
    };
  
  const rowClassName = (record, index) => {
      let className = 'lightRow';
      if(index % 2 === 1) className = 'darkRow';
      return className;
    }
    
  return (
      <div className="lessonTab-StudentList">
        <Table
          pagination={paginationProps}
          rowKey={record => record.id}
          rowSelection
          columns={columns}
          dataSource={newStudents}
          rowClassName={rowClassName}
        />
      </div>

    )
}

export default connect(
  state => ({
    newStudents:state.student
  }),
  {
    setStudents,
  }
)(Form);