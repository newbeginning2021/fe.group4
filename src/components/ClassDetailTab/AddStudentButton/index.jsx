import { connect } from 'react-redux';
import React, { useState } from 'react';
import { Modal, message, Transfer } from 'antd';
import { PlusCircleFilled} from '@ant-design/icons';
import { getEnrolledStudents,getUnEnrolledStudents,storeEnrollments } from '../../../api/api';
import './index.scss';
import '../../../styles/IconButton.scss';
import {setStudents} from '../../../state/student/actions'


const AddStudentButton = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [enrolled, setEnrolled] = useState([])
  const [unEnrolled, setUnEnrolled] = useState([])
  const [length, setLength] = useState(0)
  const { classId } = props

  const showModal = () => {
    const totalList = []
    const keyTargets = []
    let studentNum = 0;
    getEnrolledStudents(classId)
      .then(response => {
        if (response.status === 200) {
          response.data.map((item) => totalList.push({
            id: item.students.id,
            name:item.students.name,
            disabled: true
          }))
          studentNum = totalList.length
          setLength(studentNum)
        } else {
          setIsModalVisible(false)
          message.error('Internet error! Please try again', 5)
        }
      });
    getUnEnrolledStudents(classId)
      .then(response => {
        if (response.status === 200) {
          response.data.map((item) => totalList.push(
            {
              ...item,
              disabled:false
            }
          ))
          for (let i = 0; i < studentNum; i+=1){
            keyTargets.push(totalList[i].id)
          }
          setUnEnrolled(totalList)
          setEnrolled(keyTargets)
          setIsModalVisible(true);
        } else {
          setIsModalVisible(false)
          message.error({
            content: 'Internet error! Please try again',
            className:'custom-class'
          }, 5)
        }
      });
    setIsModalVisible(true)
  };

  const handleOk = () => {
    const newEnroll = enrolled.reverse().slice(length)
    if (newEnroll !== []) { 
      const enrollmentList = []
      for (let i = 0; i < newEnroll.length; i+=1) { 
        enrollmentList.push({
          student_id: newEnroll[i],
          course_id:classId
        })
      }
      storeEnrollments(enrollmentList)
        .then(async response => {
          if (response.status === 200) {
            message.success({
              content: 'Enroll success!',
              className:'custom-class'
            }, 5)
            const newData = await getEnrolledStudents(classId)
            props.setStudents(newData.data)
          } else { 
            message.error({
              content: 'Internet error! Please try again',
              className:'custom-class'
            }, 5)
          }
        })
    }
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  
  const handleChange = (targetKeys) => {
    setEnrolled(targetKeys);
  };

  const filterOption = (inputValue, option) => option.name.indexOf(inputValue) > -1;

  return (
    <div className='iconButton'>
      <button type="button" onClick={showModal}>
        <PlusCircleFilled className="iconButton__icon"/>
        Add Students
      </button>
      <Modal
        title="ENROLL STUDENTS"
        visible={isModalVisible} 
        onOk={handleOk}
        onCancel={handleCancel}
        className="studentForm"
        width={800}
      >
        <h4>Enroll Students</h4>
        <p>
          (
          Select the date on which the students
          will be enrolled in the class and then 
          select which students to enroll.
          )
        </p>
        <h5>Select students</h5>
        <Transfer
          className='transfer'
          rowKey={record => record.id}
          dataSource={unEnrolled}
          showSearch
          filterOption={filterOption}
          targetKeys={enrolled}
          onChange={handleChange}
          render={item => item.name}
        />
      </Modal>
    </div>
  )
}

export default connect(
  state => ({
    newStudents:state.student
  }),
  {
    setStudents,
  }
)(AddStudentButton);
