import { connect } from 'react-redux';
import React, { useEffect } from 'react';
import { Tabs } from 'antd';
import { TeamOutlined, ReadFilled } from '@ant-design/icons';
import moment from 'moment';
import Form from './Form';
import ClassListItem from './LessonListItem';
import IconButton from './IconButton';
import AddStudentButton from './AddStudentButton';
import { showLessons } from '../../api/api';
import './index.scss';
import { setLessons } from '../../state/lesson/actions';

const { TabPane } = Tabs;

const ClassTab = (props) => {
  const { id, newLessons } = props;
  const classId = id;

  useEffect(() => {
    showLessons(classId).then((response) => {
      if (response.status === 200) {
        props.setLessons(response.data);
      } else {
        props.setLessons('');
      }
    });
  }, []);

  return (
    <div className='classTab'>
      <Tabs className="classTab__lesson" type="card">
      <TabPane
        className="classTab__list"
        tab={
          <span>
            <ReadFilled />
            Lessons
          </span>
        }
        key="lessons"
      >
        <div>
          <h4>Lessons</h4>
          <IconButton classId={classId} schoolId="1" />
        </div>
        {newLessons.map((item) => {
          const weekday = moment(item.date).format('ddd');
          const studentsNumber = item.studentInfo?.length;
          return (
            <ClassListItem
              key={item._id}
              teacher={item.teacherInfo?.name}
              teacherId={item.teacherInfo?.id}
              img={item.teacherInfo?.img}
              students={studentsNumber}
              date={item.date}
              startTime={item.start_time}
              endTime={item.end_time}
              week={weekday}
            />
          );
        })}
      </TabPane>
      <TabPane
        className="classTab__list"
        tab={
          <span>
            <TeamOutlined />
            Students
          </span>
        }
        key="students"
      >
        <div>
          <h4>Students</h4>
          <AddStudentButton classId={classId} schoolId="1" />
        </div>
        <Form classId={classId} />
      </TabPane>
    </Tabs>
    </div>
  );
};

export default connect(
  (state) => ({
    newLessons: state.lesson,
  }),
  {
    setLessons,
  },
)(ClassTab);
