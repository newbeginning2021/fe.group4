import React from 'react';
import { Link } from 'react-router-dom';
import { faClock, faUserFriends } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import 'normalize.css';
import { Avatar, Image } from 'antd';
import 'antd/dist/antd.css';

import './index.scss';

class LessonListItem extends React.PureComponent {
  render() {
    const { teacher, students, date, week, startTime, endTime, img, teacherId } = this.props;
    return (
      <div className="lessonItem">
        <div className="lessonItem__content">
          <div className="lessonItem__teacherContainer lesson__elem">
            <div className="lessonItem__teacher">
              <div className="teacher__title">Instructor</div>
              <div className="teacher__content">
                <Avatar
                  className="test"
                  size={{ xs: 40, sm: 40, md: 50, lg: 25, xl: 25, xxl: 25 }}
                  src={<Image src={img} />}
                />
                <div className="teacher__name">
                  <Link to={`/teachers/${teacherId}`} className="lessonItem__name">
                    {teacher}
                  </Link>
                </div>
              </div>
            </div>
          </div>

          <div className="timeContainer lesson__elem">
            <div className="lessonItem__time">
              <strong>{week}</strong>
              <strong className="lessonItem__date">{date}</strong>
              <small>
                <FontAwesomeIcon icon={faClock} />
                {startTime}-{endTime}
              </small>
            </div>
          </div>

          <div className="lessonItem__bottom lesson_elem">
            <FontAwesomeIcon icon={faUserFriends} />
            <span className="lessonItem__count">{students}</span>
          </div>
        </div>
      </div>
    );
  }
}

export default LessonListItem;
