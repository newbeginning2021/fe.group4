import React from "react";
import "./index.scss"

function EmailBoxModal(props) {
  const {content} = props
  return(
    <div className="popup">
      <div className="popup__box">
        <button 
          className="popup__icon" 
          type="button" 
          onClick={() => {props.handleClose(); 
          props.handleFill()}}
        >
          x
        </button>
        {content}
      </div>
    </div>
  );
  
}

 
export default EmailBoxModal;