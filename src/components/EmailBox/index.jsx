import React, { useState, useEffect } from 'react';
import emailjs from 'emailjs-com';
import EmailBoxModal from './EmailBoxModal';
import './index.scss';

const EmailBox = (props) => {
  const { email } = props;
  const [isOpen, setIsOpen] = useState(false);
  const [toEmail, setToEmail] = useState('');
  const togglePopup = () => {
    setIsOpen(!isOpen);
  };

  const autoFill = () => {
    setToEmail(props.email);
  };
  useEffect(() => {
    autoFill();
  }, [email]);

  const sendEmail = (e) => {
    e.preventDefault();
    emailjs.sendForm('ivy_group', 'template_ivy', e.target, 'user_ncpsQ32HdcBLnIx1xHTLz');
    e.target.reset();
    togglePopup();
  };

  const onChange = (e) => {
    setToEmail(e.target.value);
  };

  return (
    <div>
      <input className="email__container" type="button" value="Send Email" onClick={togglePopup} />
      {isOpen && (
        <EmailBoxModal
          content={
            <>
              <h3 className="email__title">SEND EMAIL MESSAGE</h3>
              <form id="contact-form" onSubmit={sendEmail}>
                <div className="email__form">
                  <label htmlFor="name">
                    Subject
                    <input name="subject" type="text" className="email__subject" />
                  </label>
                </div>
                <div className="email__form">
                  <label htmlFor="exampleInputEmail1">
                    To:
                    <input
                      name="to"
                      type="email"
                      className="email__to"
                      value={toEmail}
                      onChange={onChange}
                    />
                  </label>
                </div>
                <div className="email__form">
                  <label htmlFor="message">
                    Message
                    <textarea name="message" className="email__message" rows="5" />
                  </label>
                </div>
                <div className="email__btn">
                  <button className="email__btn--send" type="submit">
                    Send
                  </button>
                  <button className="email__btn--cancel" type="button" onClick={togglePopup}>
                    Cancel
                  </button>
                </div>
              </form>
            </>
          }
          handleClose={togglePopup}
          handleFill={autoFill}
        />
      )}
    </div>
  );
};

export default EmailBox;
