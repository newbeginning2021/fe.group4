import React from 'react';
import TeacherList from './TeacherList';
import TeacherHeader from './Header';
import Header from '../Header';
import Footer from '../Footer';
import './index.scss';
import SideBar from '../SideBar/SideBar';

const TeacherPage = () => (
  <div className="layout">
    <SideBar />
    <div className="teacherPage">
      <Header />
      <div className="teacherPage__container">
        <TeacherHeader />
        <TeacherList />
      </div>
      <Footer />
    </div>
  </div>
);

export default TeacherPage;
