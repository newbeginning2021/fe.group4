import 'antd/dist/antd.css'; 
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Table, Popconfirm } from 'antd';
import { faEye, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import { setCurrentCount, setTotalCount } from '../../../state/teachersCount/actions'
import Avatar from './Avatar/index';
import ListHeader from './ListHeader/index';
import CountBar from './CountBar/index';
import {getTeacherList, deleteTeachers} from '../../../api/api'

const teacherList = (props) => {


  const pageSize = 8;
  const [data, setData] = useState([]);
  let allItem = '';
  let pageItem = '';

  const rowSelection = {
    onChange: () => {},
  };

  async function getData() {
    const response = await getTeacherList();

    setData(response.data);
  }

  const handleDelete = async (record) => {
    await deleteTeachers(record.id);
    const res = await getTeacherList();
    setData(res.data);
  };

  const columns = [
    {
      title: 'Name',
      dataIndex: 'avatar',
      render: (_, record) => (
        <Avatar url={record.img} name={record.name} dob={record.Dob} id={record.id} />
      ),
    },
    {
      title: 'Id',
      dataIndex: 'id',
    },

    {
      title: 'Phone',
      dataIndex: 'phone',
    },

    {
      title: 'Email',
      dataIndex: 'email',
    },

    {
      title: 'Address',
      dataIndex: 'address',
    },

    {
      title: 'Action',
      dataIndex: 'action',
      render: (_, record) => (
        <div className="action">
          <Link className="action__link" to={`/teachers/${record.id}`}>
            <div className="action__view-container">
              <FontAwesomeIcon className="action__viewIcon" icon={faEye} />
            </div>
          </Link>

          <div className="action__delete-container">
            <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record)}>
              <FontAwesomeIcon className="action__deleteIcon" icon={faTrash} />
            </Popconfirm>
          </div>
        </div>
      ),
    },
  ];

  const showTotalData = (totalData, range) => {
    allItem = totalData;
    pageItem = range[1] - range[0] + 1;
  };

  const rowClassName = (record, index) => {
    let className = 'lightRow';
    if (index % 2 === 1) className = 'darkRow';
    return className;
  };

  const paginationProps = {
    pageSize,
    showTotal: showTotalData,
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      if (!allItem) {
        props.setCurrentCount(0);
        props.setTotalCount(0);
      } else {
        props.setCurrentCount(pageItem);
        props.setTotalCount(allItem);
      }
    });
    return () => clearInterval(interval);
  });

  return (
    <div className="teacher__list">
      <CountBar />
      <div className="teacher__table">
        <ListHeader setData={setData} />
        <Table
          pagination={paginationProps}
          rowKey={(record) => record.id}
          rowSelection={{ ...rowSelection }}
          columns={columns}
          dataSource={data}
          rowClassName={rowClassName}
        />
      </div>
    </div>
  );
};


export default connect(
  state => ({
    currentCount : state.teachersCount.currentCount
  }),
  {
    setTotalCount,
    setCurrentCount,
  }
)(teacherList);