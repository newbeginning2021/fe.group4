import 'antd/dist/antd.css'; 
import React, { useState } from 'react';
import { Form, Input } from 'antd';
import {getTeacherList, getTeacherResults} from '../../../../../api/api';



const SearchBar = (props) => {
    const {setData} = props;
    const [keyword, setKeyword] = useState([]);

    function onChangeKeyword(e) {
        setKeyword(e.target.value)
    }

    async function getDataByKeyword() {
        let response = ""
        if(keyword === ''|| keyword instanceof(Array)) {
            response = await getTeacherList();
            setData(response.data);
        } else {
            response = await getTeacherResults(keyword);
            setData(response.data);
            
        }
    }

    return (
      <Form className='searchBar' onFinish={getDataByKeyword}>
        <Form.Item name="keyword">
          <Input.Search
            value={keyword}
            placeholder="Search by name, phone or email"
            onChange={onChangeKeyword}
            onPressEnter={getDataByKeyword}
            onSearch={getDataByKeyword}
          />
        </Form.Item>
      </Form>
      )



}

export default SearchBar