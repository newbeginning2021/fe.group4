import React from 'react';
import { faSyncAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {getTeacherList} from "../../../../api/api"
import SearchBar from './SearchBar/index';


const ListHeader = (props) => {
    const { setData } = props;

    const handleSelect = async () => {

      const res = await getTeacherList()
      setData(res.data);
    };
    return (
      <div className='list__header'>
        <SearchBar setData={setData} /> 
        <button
        type="button"
        className="countBar__refreshBtn"
        onClick={handleSelect}
        >
        <FontAwesomeIcon icon={faSyncAlt} />
      </button>
      </div>
    )
}

export default ListHeader;