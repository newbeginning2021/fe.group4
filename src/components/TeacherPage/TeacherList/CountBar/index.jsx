import { faUserCheck } from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import './index.scss';

const CountBar = (props) => {
  const { currentCount, totalCount } = props;
  return (
    <div className="page__separator countBar">
      <div className="separator__text">
        <div className="countBar__container">
          <FontAwesomeIcon icon={faUserCheck} className="countBar__icon" />
          <p>
            <span className="countBar__filteredResults">{currentCount}</span>
            <span className="countBar__divider">/</span>
            <span className="countBar__totalResults">{totalCount}</span>
            <span className="countBar__students">teachers</span>
          </p>
        </div>
      </div>
    </div>
  );
};

export default connect((state) => ({
  currentCount: state.teachersCount.currentCount,
  totalCount: state.teachersCount.totalCount,
}))(CountBar);
