import React from "react";
import { Link } from "react-router-dom";
import "./index.scss";

function Header() {
  return (
    <div className="teacher__header">
      <h1>Teachers</h1>
      <Link to="/addteacher">
          <button className="teacher__btn" type="button">
            Add Teacher
          </button>
        </Link>
    </div>
  );
}

export default Header;
