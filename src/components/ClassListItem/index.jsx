import React from 'react';
import { faClock, faUserFriends } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Avatar, Image } from 'antd';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css';
import './index.scss';

class classListItem extends React.PureComponent {
  render() {
    const { teacher, name, startTime, endTime, students, img, classId, teacherId } = this.props;
    return (
      <div className="classItem__container">
        <div className="classItem__content">
          <div className="classItem__class-info class__elem">
            <div>
              <Link to={`/classes/${classId}`} className="classItem__name">
                {name}
              </Link>
            </div>
            <div className="classItem__time">
              <FontAwesomeIcon icon={faClock} />
              <p>
                {startTime} - {endTime}
              </p>
            </div>
          </div>
          <div className="classItem__teacherContainer  class__elem">
            <div className="classItem__title">Instructor</div>
            <div className="classItem__teacher">
              <Avatar
                className="test"
                size={{ xs: 25, sm: 25, md: 25, lg: 25, xl: 25, xxl: 25 }}
                src={<Image src={img} />}
              />
              <div className="classItem__teacherName">
                <Link to={`/teachers/${teacherId}`}>
                  <span>{teacher}</span>
                </Link>
              </div>
            </div>
          </div>
          <div className="classItem__studentNum class__elem">
            <FontAwesomeIcon icon={faUserFriends} />
            <small className="classItem__count">{students}</small>
          </div>
        </div>
      </div>
    );
  }
}

export default classListItem;
