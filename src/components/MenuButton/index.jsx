import React from 'react'
import './MenuButton.scss';
import * as IconAi from 'react-icons/ai'


const BurgerMenu = ({onClick, flag}) => (
  <button 
    type='button'
    className="menu__Button" 
    onClick={onClick}
  >
    Menu 
    {' '}
    {flag? <IconAi.AiFillCaretUp /> : <IconAi.AiFillCaretDown />}
  </button>
  )


export default BurgerMenu