import { faCalendarMinus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Link } from 'react-router-dom';
import './index.scss'

function Avatar(props) {
    const { url, name, dob, id } = props;    
    return (
      <div className='name__container'>
        <img className='name__avatar' alt='avatar' src={url} />
        <div className='name__description'>
          <Link to={`/students/${id}`}>{name}</Link>
          <p><FontAwesomeIcon icon={faCalendarMinus} /><span>{dob}</span></p>
        </div> 
      </div>

    )
}
export default Avatar;