import React, {useState, useEffect} from 'react';
import {postData} from '../../api/api';
import './Avatar.scss';

const Avatar = (props) => {
  const {img, photo, setPhoto} = props;
  const [value, setValue] = useState('');
  const [body, setBody] = useState('');

  const handleOnChange = e => {
      const {files} = e.target;
      setValue(files[0].name);
      setBody(files[0]);
  }
  useEffect(async ()=>{
    if(photo){
    const data = new FormData();
    data.append("body", body);
    data.append("id",img);
    await postData(data);
    setValue('');
    setPhoto(false);}
  },[photo]);
  return(
    <div className="avatar">
      <h2 className="avatar__label">Photo</h2>
      <div className="avatar__content">
      <input className="avatar__input" readOnly value={value} />
      <label htmlFor="avatar" className="avatar__upload">
         Browse           
        <input id="avatar" type="file" file-model="image" onChange={handleOnChange} />
      </label>
      </div>
    </div>
  )
}

export default Avatar;