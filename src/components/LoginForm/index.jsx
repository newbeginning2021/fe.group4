import React,{ useState, useRef } from 'react';
import { connect } from 'react-redux';
import { useHistory,Link } from "react-router-dom";
import { storeUserName, storeUserEmail } from '../../state/user/actions'
import './index.scss';
import {getUsersLogin,reGetToken} from '../../api/api'
import {isAuthenticated,login} from '../../service/auth'

function LoginForm(props) {
  const history = useHistory();
  const [emailOrUsername, setEmailOrUsername] = useState('');
  const [password, setPassword] = useState('');
  const inputEmail = useRef();
  const inputPassword = useRef();

  const handleFormOnChange1=(e)=>{
    setEmailOrUsername(e.target.value)
  }
  const handleFormOnChange2=(e)=>{
    setPassword(e.target.value)
  }
  const inputEmailOnFocus=()=>{
  inputEmail.current.style.boxShadow="0px 0px 3px 1px #32aaff";
  }
  const inputEmailOnBlur=()=>{
  inputEmail.current.style.boxShadow="0px 0px 0px 0px #fff";
  }
  const inputPasswordOnFocus=()=>{
   inputPassword.current.style.boxShadow="0px 0px 3px 1px #32aaff";
  }
  const inputPasswordOnBlur=()=>{
   inputPassword.current.style.boxShadow="0px 0px 0px 0px #fff";
  }
  const submit=async(e)=>{
    e.preventDefault();
    const loginData = {emailOrUsername, password}
    const res = await getUsersLogin(loginData)
    const {name,email,token,school_id} = res.data;
    props.storeUserEmail(email)
    props.storeUserName(name)
    if(res.data.isUser) {
        await login(name, email, token,school_id);
      } else {
        document.querySelector('.alert').style.display="block";
      }
      if(isAuthenticated()){
        await reGetToken()
        history.push('/dashboard')
      }
  }
  
 
  return (
    <form 
      className="login-form"  
      method="POST"
    >
      <h1>Log in to ISMS</h1>
      <div className="login-form__alert">
        Invalid username or password, try again.
      </div>
      <div className="login-form__email">
        <label htmlFor="username">
          Email or Username
          <input 
            id="username" 
            type="text" 
            name="email" 
            ref={inputEmail}
            value={emailOrUsername} 
            onChange={handleFormOnChange1}
            onFocus={inputEmailOnFocus}
            onBlur={inputEmailOnBlur}
            required 
          />
        </label>
      </div>
      <div className="login-form__password">
        <label htmlFor="password">
          Password
          <input 
            className="password-input"
            id="password" 
            type="password" 
            name="password" 
            ref={inputPassword}
            value={password} 
            onChange={handleFormOnChange2}
            onFocus={inputPasswordOnFocus}
            onBlur={inputPasswordOnBlur}
            required 
          />
        </label>
      </div>
      <button type="submit" onClick={(e)=>{submit(e)}}>Log In</button>
      <p>Dont have an account? <Link to="/register">Register an Account</Link></p>
    </form>
  )
}

export default connect(
  state => ({
    userNameInfo: state.user.userName
  }),
  {
    storeUserEmail,
    storeUserName,
  }
)(
  LoginForm
)