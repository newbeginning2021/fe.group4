import React, { useState,useEffect, useRef } from 'react';
import './index.scss';
import { Link, withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';
import { message } from 'antd';
import { storeUserEmail } from '../../state/user/actions'
import {login,logout} from '../../service/auth'
import {
  lowercaseValidation,
  uppercaseValidation,
  numberValidation,
  lengthValidation,
  checkAccount,
  emailValidation,
  postUsers,
  reGetToken
} from '../../api/api';


function Register(props) {
  const [lowercase, setLowercase] = useState(false)
  const [uppercase, setUppercase] = useState(false)
  const [number, setNumber] = useState(false)
  const [length, setLength] = useState(false)
  const [email, setEmail] = useState(false)
  const [account, setAccount] = useState(false)
  const [accountName, setAccountName] = useState('')
  const [password, setPassword] = useState('')
  const [passwordValid, setPasswordValid] = useState(false)
  const [ifFill, setIfFill] = useState(false)
  const [ifPasswordFill, setIfPasswordFill] = useState(false)
  
  const accountInput = useRef()
  const passwordInput = useRef()
  const exist = useRef()
  const valid = useRef()
  const passwordHint = useRef()
  const passwordError = useRef()

  useEffect(() => {
    accountInput.current.focus()
    passwordHint.current.classList.add('hide')
    logout()
  }, [])

  const handleAccount = event => {
    const currentAccount = event.target.value;
    setAccountName(currentAccount)
    if (currentAccount !== '') {
      const emailValid = emailValidation(currentAccount);
      setIfFill(true)
      setEmail(emailValid)
      checkAccount(currentAccount)
        .then(response => {
          if (response.status === 200) {
            if (response.data.valid && emailValid) {
              accountInput.current.classList.remove('input-invalid-shadow')
              accountInput.current.classList.add('input-valid-shadow')
              exist.current.classList.remove('small-invalid')
              valid.current.classList.remove('small-invalid')
              setAccount(true)
            }
            else if (!response.data.valid && emailValid) {
              accountInput.current.classList.remove('input-valid-shadow')
              accountInput.current.classList.add('input-invalid-shadow')
              exist.current.classList.add('small-invalid')
              valid.current.classList.remove('small-invalid')
              setAccount(false)
            }
            else if (response.data.valid && !emailValid) {
              accountInput.current.classList.remove('input-valid-shadow')
              accountInput.current.classList.add('input-invalid-shadow')
              valid.current.classList.add('small-invalid')
              exist.current.classList.remove('small-invalid')
              setAccount(false)
            }
          }
        }
        )
    } else { 
      setEmail(false)
      setLowercase(false)
      setUppercase(false)
      setNumber(false)
      setLength(false)
      accountInput.current.classList.remove('input-valid-shadow')
      accountInput.current.classList.add('input-invalid-shadow')
      valid.current.classList.add('small-invalid')
      exist.current.classList.remove('small-invalid')
    }
  }

  const handleAccountFocus = () => {
    if (ifFill) {
      if (account && email) {
        accountInput.current.classList.add('input-valid-shadow')
        accountInput.current.classList.remove('input-valid')
      }
      else {
        accountInput.current.classList.add('input-invalid-shadow')
        accountInput.current.classList.remove('input-invalid')
      }
    }
  }

  const handleAccountBlur = () => {
    if (ifFill) {
      if (account && email) {
        accountInput.current.classList.remove('input-valid-shadow')
        accountInput.current.classList.add('input-valid')
      }
      else {
        accountInput.current.classList.remove('input-invalid-shadow')
        accountInput.current.classList.add('input-invalid')
      }
    }
  }

  const handlePassword = event => {
    const currentPassword = event.target.value;
    setPassword(currentPassword)
    setIfPasswordFill(true)
    if (currentPassword !== '') {
      setLowercase(lowercaseValidation(currentPassword));
      setUppercase(uppercaseValidation(currentPassword));
      setNumber(numberValidation(currentPassword));
      setLength(lengthValidation(currentPassword, 8));
      if (
        lowercaseValidation(currentPassword) &&
        uppercaseValidation(currentPassword) &&
        numberValidation(currentPassword) &&
        lengthValidation(currentPassword, 8)
      ) {
        passwordInput.current.classList.remove('input-invalid-shadow')
        passwordInput.current.classList.add('input-valid-shadow')
        passwordError.current.classList.remove('small-invalid')
        setPasswordValid(true)
      } else {
        passwordInput.current.classList.add('input-invalid-shadow')
        passwordInput.current.classList.remove('input-valid-shadow')
        passwordError.current.classList.add('small-invalid')
        setPasswordValid(false)
      }
    } else {
      passwordInput.current.classList.add('input-invalid-shadow')
      passwordInput.current.classList.remove('input-valid-shadow')
      passwordError.current.classList.add('small-invalid')
      setLowercase(false);
      setUppercase(false);
      setNumber(false);
      setLength(false);
      setPasswordValid(false)
    }
  }

  const handlePasswordFocus = () => {
    passwordHint.current.classList.add('show')
    if (ifPasswordFill) {
      if (passwordValid) {
        passwordInput.current.classList.add('input-valid-shadow')
        passwordInput.current.classList.remove('input-valid')
      }
      else {
        passwordInput.current.classList.add('input-invalid-shadow')
        passwordInput.current.classList.remove('input-invalid')
      }
    }
  }

  const handlePasswordBlur = () => {
    passwordHint.current.classList.remove('show')
    if (ifPasswordFill) {
      if (passwordValid) {
        passwordInput.current.classList.remove('input-valid-shadow')
        passwordInput.current.classList.add('input-valid')
      }
      else {
        passwordInput.current.classList.remove('input-invalid-shadow')
        passwordInput.current.classList.add('input-invalid')
      }
    }
  }

  const handleClick = async () => { 
    if (account && email && passwordValid) {
      postUsers({
        account: accountName,
        email: accountName,
        password
      }).then(response => { 
        const { token, _doc: { id } } = response?.data;
        login(accountName,accountName, token)
        reGetToken();
        props.history.push('./school', {
          id,
        })
      })
    } else {
      message.error({
        content: 'Input Error! Please check if your account and password valid',
        className: 'custom-class',
        duration: 5
      })
    }  
    const userEmail = sessionStorage.getItem('email');
    props.storeUserEmail(userEmail)
    return null;
  }

  return (
    <div className='registerForm'>
      <section >
        <h1>
          <span>start&nbsp;</span>
          using&nbsp;
          <span>ivy</span>
        </h1>
        <form>
          <div>
            <p>
              <span>enter&nbsp;</span>
              your&nbsp;
              <span>email</span>
            </p>
            <input
              ref={accountInput}
              type="text"
              onChange={handleAccount}
              onFocus={handleAccountFocus}
              onBlur={handleAccountBlur}
            />
            <small ref={exist}>
              An account with this email already exist
            </small>
            <small ref={valid}>
              Please enter a valid email address.
            </small>
          </div>
          <div>
            <p>
              <span>choose&nbsp;</span>
              a&nbsp;
              <span>password</span>
            </p>
            <p>
              <span>minimum&nbsp;</span>
              8 characters
            </p>
          </div>
          <input
            type="password"
            ref={passwordInput}
            onChange={handlePassword}
            onFocus={handlePasswordFocus}
            onBlur={handlePasswordBlur}/>
          <small ref={passwordError}>
            {
              (
                () => { 
                  if (password === '') {
                    return 'Please enter a password';
                  } if (password!=='' && length) { 
                    return 'Password must contain at least' +
                    'one numeric and one small and capital alphabetic character'
                  }
                  return 'Your password must be at least 8 characters long';
                }
              )()
            }
          </small>
          <div ref={passwordHint}>
            <p>Password must contain the following:</p>
            <ul>
              <li className={lowercase ? "li-validation" : ''}>
                {lowercase ? <CheckOutlined /> : <CloseOutlined />} 
                <span>A lowercase letter</span>
              </li>
              <li className={uppercase ? "li-validation" : ''}>
                {uppercase ? <CheckOutlined /> : <CloseOutlined />} 
                <span>A uppercase letter</span>
              </li>
              <li className={number ? "li-validation" : ''}>
                {number ? <CheckOutlined /> : <CloseOutlined />} 
                <span>A number</span>
              </li>
              <li className={length ? "li-validation" : ''}>
                {length ? <CheckOutlined /> : <CloseOutlined />} 
                <span>Minimum 8 characters</span>
              </li>
            </ul>
          </div>
            <button type="button" onClick={handleClick}>
                <span>create</span>&nbsp;
                <span>your</span>&nbsp;
                <span>free</span>&nbsp;
                <span>account</span>
            </button>
        </form>
        <p>
          By creating an account with IVY, 
          you are agreeing to our terms & conditions and our privacy policy.
        </p>
        <p>
          Already have an account?&nbsp; 
          <Link to="/login">
            <span>
            Log in here
            </span>
          </Link>
        </p>
      </section>
      <section>
        <ul>
          <li>Try out all our features completely free for 7 days</li>
          <li>No credit card details required until you upgrade to a paid plan</li>
          <li>Try our test data or jump right in and create your school</li>
          <li>
              We&apos;re always happy to provide
              support and demos to help get your school 
              up and running where required
          </li>
        </ul>
      </section>
    </div>
  )
}



export default withRouter(connect(
  state => ({
    userName: state.user.userName
  }),
  {
    storeUserEmail
  } 
  )(Register));
