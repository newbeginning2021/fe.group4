import React from 'react';
import './index.scss';
import ClassListItem from '../ClassListItem';
import { showCurrentDayClasses } from '../../api/api';
import Header from '../Header/index';
import Footer from '../Footer';
import Overview from './Overview';
import SideBar from '../SideBar/SideBar';

class DashBoard extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      infoList: [],
    };
  }

  componentDidMount() {
    showCurrentDayClasses().then((response) => {
      if (response.status === 200) {
        this.setState({
          infoList: response.data.lessons,
        });
      }
    });
  }

  render() {
    const { infoList } = this.state;
    const lessonsNumber = infoList?.length;
    return (
      <div className="layout">
        <SideBar />
        <div className="dashboard">
          <Header />
          <div className="dashboard__container">
            <Overview lessonsNumber={lessonsNumber} />
            <div className="page__separator">
              <div className="separator__text">Today Lessons</div>
            </div>
            <h6>
              THERE ARE &nbsp;
              <span>{lessonsNumber}</span>
              &nbsp; LESSONS TODAY
            </h6>
            {infoList?.map((item) => (
              <ClassListItem
                key={item._id}
                name={item.classes.name}
                classId={item.classes.id}
                teacher={item.teachers.name}
                teacherId={item.teachers.id}
                students={item.studentenrollments.length}
                startTime={item.start_time}
                endTime={item.end_time}
                img={item.teachers.img}
              />
            ))}
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}

export default DashBoard;
