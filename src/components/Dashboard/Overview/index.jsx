import React from 'react';
import { connect } from 'react-redux';
import './index.scss';

function Overview(props) {
  const { lessonsNumber, teacherCount, studentCount, classCount } = props

  return (
    <div className='overview'>
      <div className='page__separator'>
        <div className='separator__text'>Overview</div>
      </div>
      <div className="overview__container">
        <div className="overview__card">
          <div className="overview__left">
            <div className="overview__body">
              <h6>Lessons Today</h6>
              <p>{lessonsNumber}</p>
              <strong>Total Lessons</strong>
            </div>
            <div className="overview__body card--border">
              <h6>Classes</h6>
              <p>{classCount}</p>
              <strong>Total Classes</strong>
            </div>
          </div>
          <div className="overview__right">
            <div className="overview__body">
              <h6>Teachers</h6>
              <p>{teacherCount}</p>
              <strong>Total Teachers</strong>
            </div>
            <div className="overview__body card--border">
              <h6>Students</h6>
              <p>{studentCount}</p>
              <strong>Total Students</strong>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default connect(
  state => ({
    teacherCount: state.headerCount.teacherCount,
    classCount: state.headerCount.classCount,
    studentCount: state.headerCount.studentCount,
  })
)(Overview)
