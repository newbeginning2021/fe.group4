import React, { useState } from 'react';
import { Breadcrumb, notification } from 'antd';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { v4 as uuidv4 } from 'uuid';
import { Link, useHistory } from 'react-router-dom';
import { postStudents } from '../../api/api';
import Avatar from '../Avatar/Avatar';
import './index.scss';

const AddStudent = () => {
  const url = process.env.REACT_APP_URL;
  const [name, setName] = useState('');
  const [sex, setgender] = useState('male');
  const [Regidate, setRegidate] = useState('');
  const [Dob, setBirthday] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [address, setAddress] = useState('');
  const [photo, setPhoto] = useState(false);
  const [imgId] = useState(uuidv4());
  const [img] = useState(`${url}/${imgId}`);
  const history = useHistory();

  const openNotificationWithIcon = ()=>{
    notification.open({
      message: 'Saved Success',
      duration: 2,
      type:"success"
    })
  }

  const resetForm = () => {
    setName('');
    setgender('');
    setBirthday('');
    setPhone('');
    setEmail('');
    setAddress('');
    setRegidate('');
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setPhoto(true);
    const school_id = sessionStorage.getItem('school_id');
    const newStudent = {
      name,
      sex,
      Dob,
      phone,
      email,
      address,
      img,
      school_id,
    };
    await postStudents(newStudent).then(() => {
      history.push('/students');
      openNotificationWithIcon();
    });
  };

  return (
    <div className="row-fluid add-page">
      <div className="span12">
        <form method="post" className="ng-pristine ng-valid" onSubmit={handleSubmit}>
          <div className="main-header">
            <Breadcrumb separator=">">
              <Breadcrumb.Item>
                <Link to="/students">Students</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item>Add Student</Breadcrumb.Item>
            </Breadcrumb>
            <div className="page__separator">
              <div className="separator__text">Enter Student Details</div>
            </div>
          </div>
          <div className="add-page__form">
            <div>
              <fieldset>
                <div className="add-page__form__header">
                  <strong>Personal Details</strong>
                  <div role="button" tabIndex="0" onClick={resetForm} onKeyDown={resetForm}>
                    <FontAwesomeIcon icon={faTrash} />
                  </div>
                </div>
                <div className="control-group clearfix add-page__form__detail">
                  <div className="span6">
                    <label htmlFor="name">
                      <div>Name</div>
                      <input
                        id="name"
                        className="required-field span12"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        placeholder="Name"
                        type="text"
                        name="Name"
                        required="required"
                      />
                    </label>
                  </div>

                  <div className="span6">
                    <label htmlFor="gender">
                      <div>Gender</div>

                      <select
                        className="required-field span12"
                        placeholder="Gender"
                        onChange={(e) => setgender(e.target.value)}
                      >
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                      </select>
                    </label>
                  </div>
                </div>

                <div className="control-group clearfix add-page__form__detail date__detail">
                  <div className="span4">
                    <label htmlFor="registrationdate">
                      <div>Registration Date</div>

                      <input
                        id="registrationdate"
                        className="required-field span12"
                        value={Regidate}
                        onChange={(e) => setRegidate(e.target.value)}
                        type="date"
                      />
                    </label>
                  </div>

                  <div className="span4">
                    <label htmlFor="birthday">
                      <div>Birthday</div>
                      <input
                        id="birthday"
                        className="required-field span12"
                        value={Dob}
                        onChange={(e) => setBirthday(e.target.value)}
                        type="date"
                        required="required"
                      />
                    </label>
                  </div>
                </div>
                <div className="control-group clearfix add-page__form__detail">
                  <div className="add-page__form__avatar">
                    <Avatar img={imgId} photo={photo} setPhoto={setPhoto} />
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <div className="add-page__form__header">
                  <strong>Contact Details</strong>
                </div>
                <div className="control-group clearfix add-page__form__detail ">
                  <div>
                    <label htmlFor="mobilephone">
                      <div>Mobile Phone</div>
                      <input
                        id="mobilephone"
                        className="required-field span12"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                        placeholder="phone number"
                        type="text"
                        name="phone"
                      />
                    </label>
                  </div>

                  <div>
                    <label htmlFor="emailaddress">
                      <div>Email Address</div>
                      <input
                        id="emailaddress"
                        className="required-field span12"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Email address"
                        type="email"
                        name="email"
                        required="required"
                      />
                    </label>
                  </div>
                </div>
                <div className="add-page__form__address">
                  <div>
                    <label htmlFor="address">
                      <div>Address</div>
                      <textarea
                        className="required-field span12"
                        value={address}
                        onChange={(e) => setAddress(e.target.value)}
                        type="text"
                        required="required"
                      />
                    </label>
                  </div>
                </div>

                <div className="button-layout">
                  <button className="cancelBtn" type="button">
                    <Link to="/students">Cancel</Link>
                  </button>

                  <button
                    className="saveBtn"
                    type="submit"
                  >
                    Save
                  </button>
                </div>
              </fieldset>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};
export default AddStudent;
