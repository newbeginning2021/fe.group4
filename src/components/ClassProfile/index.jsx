import React from 'react';
import ClassProfileHeader from './ClassProfileHeader/index';
import ClassDetails from './ClassDetails/index';
import ClassDetailTab from '../ClassDetailTab';
import SideBar from '../SideBar/SideBar';
import Header from '../Header/index';
import './index.scss';
import Footer from '../Footer/index';

function ClassProfile(props) {
  const { id } = props;
  return (
    <div className="layout">
      <SideBar />
      <div className="page__container classProfile">
        <Header />
        <div className='classPage__wrapper'>
            <ClassProfileHeader />
            <ClassDetails id={id} />
            <ClassDetailTab id={id} />
        </div>
        <Footer />
        </div>
    </div>
  );
}

export default ClassProfile;
