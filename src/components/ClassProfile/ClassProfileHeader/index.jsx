import React from 'react';
import { Breadcrumb } from 'antd';
import { Link } from 'react-router-dom'
import './index.scss';

function ClassProfileHeader() {
  return (
    <div className="class__header">
      <Breadcrumb separator=">">
        <Breadcrumb.Item>
          <Link to="/classes">Classes</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Class Details</Breadcrumb.Item>
      </Breadcrumb>
      <h1>Class Details</h1>
    </div>
  );
}

export default ClassProfileHeader;
