import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { faUserTie,
faMapMarkerAlt, faPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getClassDetail } from '../../../api/api'

import './index.scss'

export default function ClassDetails(props) {
  const [teacherName, setTeacherName] = useState('');
  const [classroom, setClassroom] = useState('');
  const [className, setClassName] = useState('');
  const [totalLessons, setTotalLessons] = useState(0);
  
  async function getData() {
    const { id } = props
    const res = await getClassDetail(id)

    setTeacherName(res.data[0]?.teacher_details[0].name)
    setClassroom(res.data[0].classroom_details[0].name)
    setClassName(res.data[0].name)
    setTotalLessons(res.data[0].lesson_details.length)
    
    let taughtDate = moment(res.data[0].lesson_details[0].date, 'YYYY-MM-DD HH:mm:ss')
   
    res.data[0].lesson_details.forEach(lesson => {
      const date = moment(lesson.date)
      if (date.diff(taughtDate, 'days') >= 0) {
        taughtDate = date
      }
    })
  }

  useEffect(() => {
    getData()
  }, [])
    
    return (
      <div className='classDetail__list'>
        <h2>{className}</h2>
        <ul>
          <li>
            <FontAwesomeIcon icon={faUserTie} />
            Teacher:
            <span>{teacherName}</span>
          </li>
          <li>
            <FontAwesomeIcon icon={faMapMarkerAlt} />
            Classroom:  
            <span>{classroom}</span>
          </li>
          <li>
            <FontAwesomeIcon icon={faPen} />
            Total Lesson: 
            <span>{totalLessons}</span>
          </li>
        </ul>
      </div>
    )
}