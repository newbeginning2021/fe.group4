import React from 'react';
import { Link } from 'react-router-dom'
import './index.scss'
import Logo from '../../img/white.svg';

function ISMSLogo() {
    
    return (
      <Link to='/'>
        <div className="logo">
            <img src={Logo} alt="logo" />
        </div>
        <h2 className="logo__title">IVY</h2>
      </Link>
    )
}

export default ISMSLogo