import React,{useState} from 'react';
import 'antd/dist/antd.css';
import './index.scss';
import { Tabs, Divider } from 'antd';
import { getTeachersClass } from '../../../api/api'

const { TabPane } = Tabs;

function TeacherTab(props) {
    const {phone,email,Dob,id,address}=props;
    const [studentId, setStudentId] = useState(window.location.pathname.split('/')[2])
    const [studentClasses,setStudentClasses] = useState([])

    const getData = async() => {
        setStudentId(window.location.pathname.split('/')[2])
        const res = await getTeachersClass(studentId)
        if(res.data.length !== 0){
            setStudentClasses(res.data[0].enrol_details)
        }else{
            setStudentClasses(res.data)
        }
    }

    return (
      <div className="teacher-tab__container">
      <Tabs onChange={getData} type="card" className="teacher-tab__tabs">
        <TabPane tab="Profile" key="1" className="teacher-tab__profile">
          <div>
            <div>
              <h4>Mobile Phone</h4>
              {phone}
            </div>
            <div>
              <h4>Email</h4>
              {email}
            </div>
            <div>
              <h4>Date of Birth</h4>
              {Dob}
            </div>
          </div>
          <div>
            <div>
              <h4>Id. Number:</h4>
              {id}
            </div>
            <div>
              <h4>Address</h4>
              {address}
            </div>
          </div>
        </TabPane>
        <TabPane tab="Classes" key="2" className="teacher-tab__classes">
          <h3>Classes Taught</h3>
          <Divider />
          <table className="teacher-tab__table">
            <tbody>
              <tr key="title">
                <td key="1" className="teacher-tab__className"><strong>Class Name</strong></td>
                <td key="2" className="teacher-tab__classId"><strong>Class Id</strong></td>
              </tr>
              {studentClasses.length !== 0 ? studentClasses.map((course)=>
                (
                  <tr key={course.name}>
                    <td key="1">
                      <a href={`/classes/${course.id}`}>
                        {course.name}
                      </a>
                    </td>
                    <td key="2">{course.id}</td>
                  </tr>
                )
              ) : (
                <tr>
                  <td>No classes...</td>
                  <td>No classes id...</td>
                </tr>
              )}
            </tbody>
          </table>
        </TabPane>
      </Tabs>
      </div>
    )
}
export default TeacherTab
  
