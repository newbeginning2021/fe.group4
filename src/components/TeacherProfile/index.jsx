import React from "react";
import { Link } from 'react-router-dom'
import { Breadcrumb } from 'antd'
import { getTeachersId } from "../../api/api";
import "../../styles/StudentProfile.scss";
import TeacherDetail from './TeacherDetail'
import TeacherTab from "./TeacherTab";
import SideBar from "../SideBar/SideBar";
import Header from "../Header/index";
import Footer from '../Footer/index'

class TeacherProfile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const id = window.location.pathname.split("/")[2];
    getTeachersId(id).then((response) => {
      if (response.status === 200) {
        localStorage.setItem("userInfo", JSON.stringify(response.data[0]));
        this.setState({
          ...response.data[0],
        });
      }
    });
  }

  render() {
    const { name, email, age, Dob, phone, sex, img, id, address } = this.state;
    return (
      <div>
        <div className="layout">
          <SideBar />
          <main className="page__container profile-page">
            <Header />
            <section className="profile-page__container">
              <Breadcrumb separator=">">
                <Breadcrumb.Item>
                  <Link to="/teachers">Teachers</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Teacher Detail</Breadcrumb.Item>
              </Breadcrumb>
              <section className="profile-page__detail">
                <TeacherDetail
                  name={name}
                  age={age}
                  email={email}
                  Dob={Dob}
                  phone={phone}
                  sex={sex}
                  img={img}
                />
              </section>
              <section className="profile-page__tab">
                <TeacherTab
                  phone={phone}
                  email={email}
                  Dob={Dob}
                  id={id}
                  address={address}
                />
              </section>
            </section>
          <Footer />
          </main>
        </div>
      </div>
    );
  }
}

export default TeacherProfile;
