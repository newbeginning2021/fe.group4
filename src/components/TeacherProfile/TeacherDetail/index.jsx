import React from 'react';
import { faPhoneSquareAlt, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import EmailBox from '../../EmailBox';
import './index.scss';

const Profile = (props) => {
  const { img, name, sex, phone, email, age } = props;
  return (
    <div className="profile">
      <div className="profile__basic">
        <strong>Teacher Name</strong>
        <span className="profile__name">{name}</span>
        {age !== undefined ? (
          <div className="profile__age">
            <span>Age: {age}</span>
            <p>Gender: {sex}</p>
          </div>
        ) : null}
      </div>
      <div className="profile__contact">
        <strong>Contact</strong>
        <div className='profile__contact__bot'>
          <div className="profile__phone">
            <FontAwesomeIcon className="profile__icon" icon={faPhoneSquareAlt} />
            <p>{phone}</p>
          </div>
          <div className="profile__email">
            <FontAwesomeIcon className="profile__icon" icon={faEnvelope} />
            <p>{email}</p>
          </div>
        </div>
      </div>
      <div>
        <img src={img} alt="Avatar" />
        <EmailBox email={email} />
      </div>
    </div>
  );
};

export default Profile;
