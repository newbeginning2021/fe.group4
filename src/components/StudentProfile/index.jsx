import React from "react";
import {Link} from 'react-router-dom'
import {Breadcrumb} from 'antd'
import SideBar from "../SideBar/SideBar";
import { getAge, getStudentsId } from "../../api/api";
import '../../styles/StudentProfile.scss';
import Profile from "./profile"
import StudentTab from "./StudentTab"
import Header from '../Header/index';
import Footer from '../Footer/index';


class StudentProfile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const id = window.location.pathname.split('/')[2];
    getStudentsId(id).then((response) => {
      if (response.status === 200) {
        localStorage.setItem('userInfo', JSON.stringify(response.data[0]));
        const Age = getAge(response.data[0].Dob);
        this.setState({
          ...response.data[0],
          age: Age,
        });
      }
    });
  }

  render() {
    const { name, email, age, Dob, phone, sex, img, id, address } = this.state;
    return (
      <div>
        <div className="layout">
          <SideBar />
          <main className="page__container profile-page">
        <Header />
            <section className="profile-page__container">
              <Breadcrumb separator=">">
                <Breadcrumb.Item>
                  <Link to="/students">Students</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Student Detail</Breadcrumb.Item>
              </Breadcrumb>
              <section className="profile-page__detail">
                  <Profile
                    name={name}
                    age={age}
                    email={email}
                    Dob={Dob}
                    phone={phone}
                    sex={sex}
                    img={img}
                  />
              </section>
              <section className="profile-page__tab">
                <StudentTab phone={phone} email={email} Dob={Dob} id={id} address={address} />
              </section>
            </section>
          <Footer />
          </main>
        </div>
      </div>
    );
  }
}

export default StudentProfile;
