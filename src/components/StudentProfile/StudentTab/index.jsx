import React, { useState } from 'react';
import { connect } from 'react-redux';
import 'antd/dist/antd.css';
import { Tabs, Popconfirm } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import IconButton from '../../IconButton';
import { getSpecificStudentEnrollment, deleteStudentEnrollment } from '../../../api/api';
import { setClass } from '../../../state/class/actions';
import './index.scss';

const { TabPane } = Tabs;

function StudentTab(props) {
  const { phone, email, Dob, id, address, newClasses } = props;
  const [studentId, setStudentId] = useState(window.location.pathname.split('/')[2]);

  const getData = async () => {
    setStudentId(window.location.pathname.split('/')[2]);
    const res = await getSpecificStudentEnrollment(studentId);
    if (res.data.length !== 0) {
      props.setClass(res.data[0].enrol_details);
    } else {
      props.setClass(res.data);
    }
  };

  const deleteClass = async (value) => {
    await deleteStudentEnrollment(value);
    await getData();
  };

  return (
    <div className="student-tab__container">
      <Tabs onChange={getData} type="card" className="student-tab__tabs">
        <TabPane tab="Profile" key="1" className="student-tab__profile">
          <div className="profileTab-first">
            <div>
              <h4>Mobile Phone</h4>
              {phone}
            </div>
            <div>
              <h4>Email</h4>
              {email}
            </div>
            <div>
              <h4>Date of Birth</h4>
              {Dob}
            </div>
          </div>
          <div>
            <div>
              <h4>Id. Number:</h4>
              {id}
            </div>
            <div>
              <h4>Address</h4>
              {address}
            </div>
          </div>
        </TabPane>
        <TabPane tab="Classes" key="2" className="student-tab__classes">
          <div className='student-tab__header'>
            <h3>Course Enrollment</h3>
            <IconButton />
          </div>
          <table className="student-tab__table">
            <tbody>
              <tr key="title">
                <td key="1" className="student-tab__className">
                  <strong>Course</strong>
                </td>
                <td key="2" className="student-tab__classId">
                  <strong>Course Id</strong>
                </td>
                <td key="3" className="student-tab__action">
                  <strong>Action</strong>
                </td>
              </tr>
              {newClasses.length !== 0 ? (
                newClasses.map((course) => (
                  <tr key={course.class_details.name}>
                    <td key="1">
                      <a href={`/classes/${course.class_details.id}`}>
                        {course.class_details.name}
                      </a>
                    </td>
                    <td key="2">{course.class_details.id}</td>
                    <td key="3">
                      <div className="student-tab__deleteIconContainer">
                        <Popconfirm
                          title="Sure to delete?"
                          onConfirm={() => {
                            deleteClass(course.enrol_id);
                          }}
                        >
                          <FontAwesomeIcon className="student-tab__deleteIcon" icon={faTrash} />
                        </Popconfirm>
                      </div>
                    </td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td>No classes...</td>
                  <td>No classes id...</td>
                  <td>...</td>
                </tr>
              )}
            </tbody>
          </table>
        </TabPane>
      </Tabs>
    </div>
  );
}

export default connect(
  (state) => ({
    newClasses: state.classes,
  }),
  {
    setClass,
  },
)(StudentTab);
