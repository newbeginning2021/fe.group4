import React, { useState, useRef, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { Link, NavLink } from 'react-router-dom';
import Logo from './Logo/index';
import UserInfo from './UserInfo/index';
import './index.scss';
import BigLogo from '../../img/white.svg';
import sidebarData from '../SideBar/SideBarData';
import { breakPoint } from '../../config/confg';

const Header = () => {
  const [menu, setMenu] = useState('');
  const myMenu = useRef();

  const handleResize = () => {
    const { innerWidth } = window
    if (innerWidth >= breakPoint
    ) { 
      myMenu.current.classList.remove('show');
      setMenu('');
    }
  }
  const handleClick = () => {
    if (menu === '') {
      myMenu.current.classList.add('show');
      setMenu('show');
    } else {
      myMenu.current.classList.remove('show');
      setMenu('');
    }
  };

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  },[]);

  return (
    <header className="header__wrapper">
      <div className="header__inner">
        <div className="header__menu">
          <div aria-hidden="true" className="header__icon" onClick={handleClick}>
            <FontAwesomeIcon icon={faBars} />
          </div>
          <div className="header__avatar">
            <Link to="/">
              <img src={BigLogo} alt="luma logo" />
            </Link>
          </div>
        </div>
        <Logo />
        <UserInfo />
        <ul ref={myMenu}>
          {sidebarData.map((item) => (
            <li
              key={item.index}
              aria-hidden="true"
            >
              <NavLink
                style={{ textDecoration: 'none' }}
                to={item.link}
                activeClassName="active"
              >
                <span>{item.icon}</span>
                <span>{item.title}</span>
              </NavLink>
            </li>
          ))}
        </ul>
      </div>
    </header>
  );
};

export default Header;
