import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { faDesktop, faBookReader } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getClassesInfo, getStudents, getTotalTeachers } from '../../../api/api';
import { setClassCount, setStudentCount, setTeacherCount } from '../../../state/headerCount/actions'
import './index.scss';

const Logo = (props) => {
  const { classCount, teacherCount } = props
  
  const getClassNum = async () => {
    const res = await getClassesInfo();
    props.setClassCount(res.data.length)
  };

  const getTotalStudents = async () => {
    const res = await getStudents();
    props.setStudentCount(res.data.length)
  };

  const getTeachers = async () => {
    const res = await getTotalTeachers();
    props.setTeacherCount(res.data.length)
  };

  useEffect(() => {
    getClassNum();
    getTotalStudents();
    getTeachers();
  }, []);

  return (
    <>
      <div>
        <div className="logo__container">
          <span className="logo__header">
            <span className="logo__icon">
              <FontAwesomeIcon icon={faDesktop} />
            </span>
            <small className="logo__header--flex">
              <strong>Classes</strong>
              <span>{classCount}</span>
            </small>
          </span>
          <span className="logo__header">
            <span className="logo__icon">
              <FontAwesomeIcon icon={faBookReader} />
            </span>
            <small className="logo__header--flex">
              <strong>Teachers</strong>
              <span>{teacherCount}</span>
            </small>
          </span>
        </div>
      </div>
    </>
  );
};

export default connect(
  state => ({
    teacherCount: state.headerCount.teacherCount,
    classCount: state.headerCount.classCount,
  }),
  {
    setTeacherCount,
    setClassCount,
    setStudentCount
  }
)(Logo);
