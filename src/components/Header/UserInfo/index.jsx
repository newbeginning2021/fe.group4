import React, { useState } from 'react';
import { connect } from 'react-redux';
import { faPowerOff } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import { logout } from '../../../service/auth';
import './index.scss';

const UserInfo = (props) => {
  const { userNameInfo, userEmailInfo } = props;
  const subStr = userNameInfo?.substring(0, 2);
  const [clicked, setClicked] = useState(false);

  function handleOnClick() {
    setClicked(!clicked);
    const dropdown = document.querySelector('.user__container');

    if (!clicked) {
      dropdown.classList.add('show');
    } else {
      dropdown.classList.remove('show');
    }
  }

  return (
    <div className="user">
      <div onClick={handleOnClick} aria-hidden="true" className="user__avatar">
        <span >{subStr}</span>
      </div>

      <ul className="user__container">
        <li className="user__header">
          <p className="user__name">{userNameInfo}</p>
          <p>{userEmailInfo}</p>
        </li>
        <li className="user__logout">
          <FontAwesomeIcon icon={faPowerOff} />
          <Link to="/login" onClick={logout}>
            Log out
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default connect((state) => ({
  userNameInfo: state.user.userName,
  userEmailInfo: state.user.userEmail,
}))(UserInfo);
