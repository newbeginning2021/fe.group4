const isAuthenticated = () => {
    if(sessionStorage.getItem('jwtToken')) return true
    return false
}

const getToken = () => sessionStorage.getItem('jwtToken')

const login = (username, email, token,school_id) => {
    sessionStorage.setItem('email', email)
    sessionStorage.setItem('username', username)
    sessionStorage.setItem('jwtToken', token)
    sessionStorage.setItem('school_id', school_id)
}

const logout = () => {
    sessionStorage.clear()
}

export {
    isAuthenticated,
    getToken,
    login,
    logout,
}
