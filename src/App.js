import React, { useEffect } from "react";
import { Route, Switch } from "react-router";
import { connect } from 'react-redux';
import { storeUserEmail, storeUserName } from './state/user/actions'
import ErrorPage from "./pages/ErrorPage"
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import Dashboard from './pages/Dashboard';
import StudentPage from './pages/StudentPage/index';
import TeacherPage from './pages/TeacherPage/index'
import ClassPage from './pages/ClassPage';
import ProtectedRoute from './components/ProtectedRoute'
import StudentProfilePage from './pages/StudentProfilePage'
import TeacherProfilePage from './pages/TeacherProfilePage'
import AddStudentPage from "./pages/AddStudentPage";
import AddTeacherPage from "./pages/AddTeacherPage";
import AddClassPage from "./pages/AddClassPage"
import ClassDetailPage from './pages/ClassDetailPage';
import SchoolPage from './pages/SchoolPage';
import Header from './components/Header/index';
import Home from './components/Home/index';
import CalendarPage from './pages/CanlendarPage'
import './App.css'
import './styles/main.scss'

function App(props) {
 
  useEffect(() => {
    const userName = sessionStorage.getItem('username');
    const userEmail = sessionStorage.getItem('email');
    props.storeUserName(userName);
    props.storeUserEmail(userEmail)
  },[])

  return (
    <>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/login" component={LoginPage} />
        <Route path="/register" component={RegisterPage} />
        <ProtectedRoute path="/school" component={SchoolPage} />
        <ProtectedRoute path="/header" component={Header} />
        <ProtectedRoute path="/dashboard" exact component={Dashboard} />
        <ProtectedRoute path="/students" exact component={StudentPage} />
        <ProtectedRoute
          path="/students/:id"
          exact
          component={StudentProfilePage}
        />
        <ProtectedRoute path="/teachers" exact component={TeacherPage} />
        <ProtectedRoute
          path="/teachers/:id"
          exact
          component={TeacherProfilePage}
        />
        <ProtectedRoute path="/classes" exact component={ClassPage} />
        <ProtectedRoute path="/classes/:id" component={ClassDetailPage} />
        <ProtectedRoute path="/addstudent" component={AddStudentPage} />
        <ProtectedRoute path="/calendar" component={CalendarPage} />
        <ProtectedRoute path="/addteacher" component={AddTeacherPage} />
        <ProtectedRoute path="/addclass" component={AddClassPage} />
        <ProtectedRoute component={ErrorPage} />
      </Switch>
    </>
  );
}


export default connect(
  state => ({
    userName: state.user.userName,
    userEmail: state.user.userEmail
  }),
  {
    storeUserName,
    storeUserEmail,
  }
)(App);